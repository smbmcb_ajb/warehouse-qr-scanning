<?php

include '../1Connection.php';

?>


<div style="padding:20px;">

    <h2>IQC Data List</h2>

    <!-- <div class="ui icon input">
        <input id="find_iqc" type="search" placeholder="Search...">
        <i class="search icon"></i>
    </div> -->

    <div class="ajax_for_iqc_search"></div>

    <table id="all_iqc_data" class="table w3-table-all w3-hoverable ui striped table">

        <thead class="report_table_head">
            <tr>
                <th>invoice number</th>
                <th>goods code</th>
                <th>product name</th>
                <th>inspection type</th>
                <th>invoice quantity</th>
                <th>status</th>
                <th>lot number</th>
            </tr>
        </thead>

        <tbody class="report_table_body">
                
            <?php

                $query = "SELECT DISTINCT
                inspectiondata.id,
                inspectiondata.invoice_no, 
                inspectiondata.goodsCode, 
                inspectiondata.inspection_type, 
                inspectiondata.invoicequant,
                tblOverall_Judgement.overall_judgement,
                tblOverall_Judgement.MaterialCodeBoxSeqID,
                LotNumber.lot_no
                FROM ((inspectiondata
                INNER JOIN tblOverall_Judgement 
                ON inspectiondata.MaterialCodeBoxSeqID = tblOverall_Judgement.MaterialCodeBoxSeqID AND
                inspectiondata.invoice_no = tblOverall_Judgement.invoice_no)
                INNER JOIN LotNumber ON LotNumber.MaterialCodeBoxSeqID = inspectiondata.MaterialCodeBoxSeqID AND
                LotNumber.invoice_no = inspectiondata.invoice_no)
                ORDER BY inspectiondata.id DESC";
                $result = sqlsrv_query($connIQC, $query);

                while($rows=sqlsrv_fetch_array($result)){

                                                            
                    echo "<tr>
                            <td>" . $rows['invoice_no'] . "</td>
                            <td>" . $rows['goodsCode'] . "</td>
                            <td>" . $rows['MaterialCodeBoxSeqID'] . "</td>
                            <td>" . $rows['inspection_type'] . "</td>
                            <td>" . $rows['invoicequant'] . "</td>
                            <td>" . $rows['overall_judgement'] . "</td>
                            <td>" . $rows['lot_no'] . "</td>
                        </tr>";
                }

            ?>

        </tbody>

    </table>

</div>


<div style="padding:20px;" class="close">
    <table id="default_table" class="table w3-table-all w3-hoverable ui striped table">

        <h2>IQC Rejection Records</h2>

        <thead class="report_table_head">
            <tr>
                <th>invoice number</th>
                <th>goods code</th>
                <th>reject</th>
                <th>invoice quantity</th>
                <th>lot number</th>
                
            </tr>
        </thead>

        <tbody class="report_table_body">
                
            <?php

                $query = "SELECT invoice_no, goodsCode, reject, total_quantity, lot_no 
                FROM [dbo].[LotNumber]
                WHERE reject != '0'
                ORDER BY id DESC";
                $result = sqlsrv_query($connIQC, $query);

                while($rows=sqlsrv_fetch_array($result)){

                                                            
                    echo "<tr>
                            <td>" . $rows['invoice_no'] . "</td>
                            <td>" . $rows['goodsCode'] . "</td>
                            <td>" . $rows['reject'] . "</td>
                            <td>" . $rows['total_quantity'] . "</td>
                            <td>" . $rows['lot_no'] . "</td>
                        </tr>";
    }

            ?>

        </tbody>

    </table>

</div>

<!-- SCRIPT FOR DATATABLE -->
<script>
    $('#all_iqc_data').DataTable({ 
        "ordering": false,
        "lengthChange": false, // disable the length 10/25/50/100
        "pagingType": "simple_numbers", //paging type numbers/simple/simple_numbers/full/full_numbers/first_last_numbers
        "pageLength": 12,
        // "searching": false
    });
</script>

<script>
    var search = '';

    $("#find_iqc").on("input", function() {

        search = jQuery(this).val(); //or this.value
        console.log(search);

        if(search != '' ){

            $('#all_iqc_data').addClass('close');
            $('.ajax_for_iqc_search').removeClass('close');

            $.ajax({
                type: 'POST',
                url: '../Ajax_IQC/ajax_iqc_search.php',
                data: {
                    search: search

                },
                cache: false,
                success: function(data) {
                    // alert(data);
                    // $('.ajax_report').empty(urlReport);
                    $('.ajax_for_iqc_search').html(data);
                },
                error: function(xhr, status, error) {
                    console.error(xhr);
                }
            });

            
        }
        else{

            $('#all_iqc_data').removeClass('close');
            $('.ajax_for_iqc_search').addClass('close');

            console.log('PUTA KA');
            
        }

    });
</script>
