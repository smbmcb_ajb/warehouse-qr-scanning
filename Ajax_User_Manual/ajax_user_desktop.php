<div class="container user_manual">

    <div class="Col-1-50 right_div">

        <!-- RECEIVING USER MANUAL STEP BY STEP -->
        <div class="column manual_card">
            <div class="ui raised segment">
                <span class="ui blue ribbon label"><i class="fa-solid fa-warehouse"></i> Receiving</span>
                <strong><span>Material Receiving Via QR Scanner / Manual Input</span></strong>

                <div class="container" style="margin-top:15px;">

                    <div class="Col-1-50" style="padding:5px;">
                        <h5>Through Scanning</h5>
                        <p>
                            <div class="ui vertical steps">
                                <div class="step">
                                    <i class="icon">
                                        <i class="fa-solid fa-arrow-pointer px28"></i>
                                    </i>
                                    <div class="content">
                                        <div class="title manual_step">STEP 1 - Select Receiving.</div>
                                        <div class="description">On the top menu with the warehouse icon, select "Receiving."</div>
                                    </div>
                                </div>
                                <div class="step">
                                    <i class="icon">
                                        <i class="fa-solid fa-camera px28"></i>
                                    </i>
                                    <div class="content">
                                        <div class="title manual_step">STEP 2 - Activate the camera</div>
                                        <div class="description">To open the camera and begin scanning, click or touch the blue button with a QR CODE icon.</div>
                                        </div>
                                </div>
                                <div class="step">
                                    <i class="icon">
                                        <i class="fa-solid fa-qrcode px28"></i>
                                    </i>
                                    <div class="content">
                                        <div class="title manual_step">STEP 3 - QR Code Alignment</div>
                                        <div class="description">To detect the QR code, align it with the camera frame, and the corresponding data will be shown.</div>
                                    </div>
                                </div>
                                <div class="step">
                                    <i class="icon">
                                        <i class="fa-solid fa-keyboard px28"></i>
                                    </i>
                                    <div class="content">
                                        <div class="title manual_step">STEP 4 - Invoice Number and Quantity</div>
                                        <div class="description">After entering the quantity and invoice number, click the "UPLOAD" button.</div>
                                    </div>
                                </div>
                            </div>
                        </p>
                    </div>

                    <div class="Col-1-50" style="padding:5px;">
                        <h5>Using the Manual Type</h5>

                        <p>
                            <div class="ui vertical steps">
                                <div class="step">
                                    <i class="icon">
                                        <i class="fa-solid fa-arrow-pointer px28"></i>
                                    </i>
                                    <div class="content">
                                        <div class="title manual_step">STEP 1 - Select Receiving.</div>
                                        <div class="description">On the top menu with the warehouse icon, select "Receiving."</div>
                                    </div>
                                </div>
                                <div class="step">
                                    <i class="icon">
                                        <i class="fa-solid fa-keyboard px28"></i>
                                    </i>
                                    <div class="content">
                                        <div class="title manual_step">STEP 2 - Enter the Item or Goods Code.</div>
                                        <div class="description">Type the Goods Code or Item Code in the textbox labeled "QR Detected / Input" to see the data associated with the input text.</div>
                                        </div>
                                </div>
                                <div class="step">
                                    <i class="icon">
                                        <i class="fa-solid fa-keyboard px28"></i>
                                    </i>
                                    <div class="content">
                                        <div class="title manual_step">STEP 3 - Invoice Number and Quantity</div>
                                        <div class="description">After entering the quantity and invoice number, click the "UPLOAD" button.</div>
                                    </div>
                                </div>
                            </div>
                        </p>
                    </div>

                </div>

            </div>
        </div>

        <div class="column manual_card close">
            <div class="ui raised segment">
                <span class="ui blue ribbon label"><i class="fa-solid fa-file-arrow-up"></i> Import</span>
                <span>Import Excel File for One time upload</span>
                <h5>Excel File</h5>
                <p></p>
            </div>
        </div>

    </div>

    <div class="Col-2-50 left_div">

        <!-- REPORT USER MANUAL TABULAR AND CARD FORMAT -->
        <div class="column manual_card">
            <div class="ui raised segment">
                <span class="ui blue_conf ribbon label"><i class="fa-solid fa-chart-simple"></i> Reports</span>
                <strong><span>View Reports "Tabular / Card" Format</span></strong>
                <p style="margin-top:10px; padding:0 5px;">All data received via Receiving or Import is documented in the Report menu and can be viewed in Tabular or Card Format.</p>

                <div class="container" style="margin-top:15px;">

                    <div class="Col-1-50" style="padding:5px;">

                        <h5>Tabular Layout</h5>
                        <img class="guide_image" src="../images/guide/tabular.png" alt="">
                        <p>
                            <div class="ui vertical steps">
                                <div class="step">
                                    <i class="icon">
                                        <i class="fa-solid fa-calendar-days px28"></i>
                                    </i>
                                    <div class="content">
                                        <div class="title manual_step"></div>
                                        <div class="description">Each month had a corresponding color to quickly identify when this data was recorded or received in Tabular Layout.</div>
                                    </div>
                                </div>
                                <div class="step">
                                    <i class="icon">
                                        <i class="fa-solid fa-square-caret-down px28"></i>
                                    </i>
                                    <div class="content">
                                        <div class="title manual_step"></div>
                                        <div class="description">You may also examine all of the report data by selecting a month from the dropdown month picker on the left side of the search input area.</div>
                                    </div>
                                </div>
                                <div class="step">
                                    <i class="icon">
                                        <i class="fa-solid fa-magnifying-glass px28"></i>
                                    </i>
                                    <div class="content">
                                        <div class="title manual_step"></div>
                                        <div class="description">You may also examine all of the report data by selecting a month from the dropdown month picker on the left side of the search input area.</div>
                                    </div>
                                </div>
                            </div>
                        </p>
                    </div>

                    <div class="Col-1-50" style="padding:5px;">
                        <h5>Format: Card</h5>
                        <img class="guide_image" src="../images/guide/Card.png" alt="">

                        <p>
                            <div class="ui vertical steps">
                                <div class="step">
                                    <i class="icon">
                                        <i class="fa-solid fa-clone px28"></i>
                                    </i>
                                    <div class="content">
                                        <div class="title manual_step"></div>
                                        <div class="description">Card Format is the default in the Android browser; however, you can view it in the desktop browser by clicking the "Card Format" button.</div>
                                    </div>
                                </div>
                                <div class="step">
                                    <i class="icon">
                                        <i class="fa-solid fa-file-pen px28"></i>
                                    </i>
                                    <div class="content">
                                        <div class="title manual_step"></div>
                                        <div class="description">If something goes wrong with the Card Format, you can update the report. Keep in mind that the developer has access to the list of users who can edit the record and report to the administrator.</div>
                                        </div>
                                </div>
                                <div class="step">
                                    <i class="icon">
                                        <i class="fa-solid fa-trash px28"></i>
                                    </i>
                                    <div class="content">
                                        <div class="title manual_step"></div>
                                        <div class="description">If you're an admin, you can erase an entire card if the Goods Code or Item Code for the quantity and invoice number is incorrect.</div>
                                    </div>
                                </div>
                            </div>
                        </p>
                    </div>

                </div>

            </div>


            
        </div>

        <div class="column manual_card close">
            <div class="ui raised segment">
                <span class="ui teal ribbon label"><i class="fa-solid fa-users"></i> User / Admin</span>
                <span>Admin and User Role</span>
                <h5>Admin</h5>
                <p></p>
            </div>
        </div>

    </div>

</div>