<?php

include '../1Connection.php';

$id_num = $_POST['id_num'] ?? '';

$query = "SELECT * FROM [dbo].[Receive] 
WHERE id = '$id_num'";
$result = sqlsrv_query($conn, $query);

while($rows=sqlsrv_fetch_array($result)){

    echo "<div class='report_card'>

            <table>
                <tr>
                    <td class='transac_id'>Transaction ID: </td>
                    <td class='transac_id'> " . $rows['id'] . "</td>
                </tr>
                <tr>
                    <td class='report_label'> Date Receive: </td>
                    <td class='report_value'> ". $rows['DATE_RECEIVE']->format('Y-m-d') . "</td>
                </tr>
                <tr>
                    <td class='report_label'> Goods Code: </td>
                    <td style='font-weight:700;' id='goods". $rows['id'] ."' class='report_value'>". $rows['GOODS_CODE'] . "</td>
                </tr>
                <tr>
                    <td class='report_label'> Invoice No.: </td>
                    <td style='font-weight:700;' class='report_value'> <input id='edit_invoice' class='input_edit' type='text' value='".$rows['INVOICE']."'></td>
                </tr>
                <tr>
                    <td class='report_label'> Quantity: </td>
                    <td style='font-weight:700;' class='report_value'> <input id='edit_qty' class='input_edit' type='text' value='".$rows['QTY']."'></td>
                </tr>
                <tr>
                    <td class='report_label'> Assy Line: </td>
                    <td class='report_value'> <input id='edit_assy_line' class='input_edit' type='text' value='".$rows['ASY_LINE']."'></td>
                </tr>
                <tr>
                    <td class='report_label'> Supplier: </td>
                    <td class='report_value'> ". $rows['SUPPLIER'] . "</td>
                </tr>
                
                <tr>
                    <td class='report_label'> Materials Type: </td>
                    <td id='materials". $rows['id'] ."' class='report_value'>". $rows['MATERIALS_TYPE'] . "</td>
                </tr>
                <tr>
                    <td class='report_label'> Item Code: </td>
                    <td id='item". $rows['id'] ."' class='report_value'>". $rows['ITEM_CODE'] . "</td>
                </tr>
                
                
                <tr>
                    <td class='report_label'> Purchase Slip: </td>
                    <td class='report_value'> <input id='edit_p_slip' class='input_edit' type='text' value='".$rows['P_SLIP']."'></td>
                </tr>
                <tr>
                    <td class='report_label'> Sales Slip: </td>
                    <td class='report_value'> <input id='edit_s_slip' class='input_edit' type='text' value='".$rows['S_SLIP']."'></td>
                </tr>
                <tr>
                    <td class='report_label'> Part Number: </td>
                    <td class='report_value'> ". $rows['PART_NUMBER'] . "</td>
                </tr>
                <tr>
                    <td class='report_label'> Part Name: </td>
                    <td class='report_value'> ". $rows['PART_NAME'] . "</td>
                </tr>
                <tr>
                    <td style='text-align:center;'> <button id='submitid".$rows['id']."' class='submit'> <i class='fa-solid fa-check'></i> Submit</button> </td>
                    <td style='text-align:center;'> <button id='cancel' class='delete'> <i class='fa-solid fa-ban'></i> Cancel</button> </td>
                </tr>
            </table>

          </div>
          
          <script>
            $('#submitid".$rows['id']."').click(function(){

                var old_qty = ".$rows['QTY'].";

                var id_num = ".$rows['id'].";
                var new_assy = $('#edit_assy_line').val();
                var new_qty = $('#edit_qty').val();
                var new_invoice = $('#edit_invoice').val();
                var new_p_slip = $('#edit_p_slip').val();
                var new_s_slip = $('#edit_s_slip').val();

                var goods = document.getElementById('goods".$rows['id']."');
                var goods_value = goods.textContent;

                var item = document.getElementById('item".$rows['id']."');
                var item_value = item.textContent;

                var materials = document.getElementById('materials".$rows['id']."');
                var materials_value = materials.textContent;

                // var urlReport = '../Ajax_Report/report.php';
                // var urlEdit = '../Ajax_Report/edit_report.php';

                $.ajax({
                    type: 'POST',
                    url: '../Ajax_Report/update_report.php',
                    data: {
                        id_num: id_num,
                        new_assy: new_assy,
                        new_qty: new_qty,
                        new_invoice: new_invoice,
                        new_p_slip: new_p_slip,
                        new_s_slip: new_s_slip,
                        goods_value: goods_value,
                        item_value: item_value,
                        old_qty: old_qty,
                        materials_value: materials_value

                    },
                    cache: false,
                    success: function(data) {
                        
                        alert(data);
                        $('.ajax_report').load(urlReport);
                        // $('.ajax_report').html(data);
                    },
                    error: function(xhr, status, error) {
                        console.error(xhr);
                    }

                });

            });
          </script>";
}

?>

<script>
    $("#cancel").click(function(){

        var urlReport = '../Ajax_Report/report.php';

        $('.ajax_report').load(urlReport);

    });
</script>