<?php

include '../1Connection.php';
include '../main_function/login.php';

$currentYear = date('Y');
$currentMonth = date('m');
$currentDay = date('d');
$EndDayOfTheMonth = '';

$picker = $_POST['picker'] ?? '';

if($picker == '02'){
    $EndDayOfTheMonth = 28;
}
elseif($picker == '04' ||
       $picker == '06' ||
       $picker == '09' ||
       $picker == '11'){
    $EndDayOfTheMonth = 30;
}
else{
    $EndDayOfTheMonth = 31;
}

$query = "SELECT * FROM [dbo].[Receive] 
WHERE ARCHIVE = '0' 
AND DATE_RECEIVE >= '$currentYear-$picker-01'
AND DATE_RECEIVE <= '$currentYear-$picker-$EndDayOfTheMonth'
ORDER BY id DESC";
$result = sqlsrv_query($conn, $query);

?>

<table id="month_report" class="table w3-table-all w3-hoverable ui striped table">

    <thead class="report_table_head">
        <tr>
            <th class="close">status</th>
            <th>PURCHASE SLIP</th>
            <th>SALES SLIP</th>
            <th>goods code</th>
            <th>invoice</th>
            <th>quantity</th>
            <th>P.O</th>
            <th>item code</th>
            <th>assy line</th>
            <th>supplier</th>
            <th class="close">materials type</th>
            <th>part number</th>
            <th>part name</th>
            <th>date receive</th>
            <th class="close">action</th>
        </tr>
    </thead>

    <tbody class="report_table_body">
            
        <?php

            while($rows=sqlsrv_fetch_array($result)){

                $color = "#fff";
                // $color = "#000";

                if($rows['DATE_RECEIVE']->format('m') == '01'){
                        $bg = "#0000FF";
                    // $bg = "#5265B2";
                }
                elseif($rows['DATE_RECEIVE']->format('m') == '02'){
                    $bg = "#8F00FF";
                    // $bg = "#C199CE";
                }
                elseif($rows['DATE_RECEIVE']->format('m') == '03'){
                    // $bg = "orange";
                    $bg = "#F47F39";
                }
                elseif($rows['DATE_RECEIVE']->format('m') == '04'){
                    // $bg = "gray";
                    $bg = "#A2B2AC";
                    $color = "#000";
                }
                elseif($rows['DATE_RECEIVE']->format('m') == '05'){
                    $bg = "#4C9A2A";
                    // $bg = "#7DB166";
                }
                elseif($rows['DATE_RECEIVE']->format('m') == '06'){
                    // $bg = "black";
                    $bg = "#0D0C12";
                }
                elseif($rows['DATE_RECEIVE']->format('m') == '07'){
                    $bg = "#FFC0CB";
                    // $bg = "#E9B7C3";
                    $color = "#000";
                }
                elseif($rows['DATE_RECEIVE']->format('m') == '08'){
                    $bg = "#964B00";
                    // $bg = "#9E8748";
                    // $color = "#000";
                }
                elseif($rows['DATE_RECEIVE']->format('m') == '09'){
                    // $bg = "yellow";
                    $bg = "#FED758";
                    $color = "#000";
                }
                elseif($rows['DATE_RECEIVE']->format('m') == '10'){
                    $bg = "#7AD7F0";
                    // $bg = "#9AB9D6";
                    $color = "#000";
                }
                elseif($rows['DATE_RECEIVE']->format('m') == '11'){
                    // $bg = "#fff";
                    $bg = "#FFFDFA";
                    $color = "#000";
                }
                else{
                    $bg = "#880808";
                    // $bg = "#DB937B";
                }

                if($rows['STATUS'] == 'RAW'){
                    $status = '#880808';
                }
                else{
                    $status = 'green';
                }
                                                        
                echo "<tr style='color:".$color."; background:".$bg.";'>
                        <td class='close' style='color:#fff; background:".$status.";'>" . $rows['STATUS'] . "</td>
                        <td>" . $rows['P_SLIP'] . "</td>
                        <td>" . $rows['S_SLIP'] . "</td>
                        <td>" . $rows['GOODS_CODE'] . "</td>
                        <td>" . $rows['INVOICE'] . "</td>
                        <td>" . $rows['QTY'] . "</td>
                        <td>" . $rows['PO'] . "</td>
                        <td>" . $rows['ITEM_CODE'] . "</td>
                        <td>" . $rows['ASY_LINE'] . "</td>
                        <td>" . $rows['SUPPLIER'] . "</td>
                        <td class='close'>" . $rows['MATERIALS_TYPE'] . "</td>
                        <td>" . $rows['PART_NUMBER'] . "</td>
                        <td>" . $rows['PART_NAME'] . "</td>
                        <td>" . $rows['DATE_RECEIVE']->format('Y-m-d') . "</td>
                        <td class='close'>
                            <button class='report_table_action edit_desktop' id=desktop_edit". $rows['id'] ."><i class='fa-solid fa-pen-to-square'></i> EDIT</button> 
                            <button class='report_table_action delete_desktop' id=desktop_delete". $rows['id'] ."><i class='fa-solid fa-trash'></i> DELETE</button>
                        </td>
                    </tr>";
            }

        ?>

    </tbody>

</table>