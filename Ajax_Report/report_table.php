<?php

include '../1Connection.php';
include '../main_function/login.php';

$currentYear = date('Y');
$currentMonth = date('m');
$currentDay = date('d');
$EndDayOfTheMonth = '';
$MonthName = date("F", mktime(0, 0, 0, $currentMonth, 10));

if ($MonthName == 'January') { $selected_01='selected'; }
elseif ($MonthName == 'February') { $selected_02='selected'; }
elseif ($MonthName == 'March') { $selected_03='selected'; }
elseif ($MonthName == 'April') { $selected_04='selected'; }
elseif ($MonthName == 'May') { $selected_05='selected'; }
elseif ($MonthName == 'June') { $selected_06='selected'; }
elseif ($MonthName == 'July') { $selected_07='selected'; }
elseif ($MonthName == 'August') { $selected_08='selected'; }
elseif ($MonthName == 'September') { $selected_09='selected'; }
elseif ($MonthName == 'October') { $selected_10='selected'; }
elseif ($MonthName == 'November') { $selected_11='selected'; }
elseif ($MonthName == 'December') { $selected_12='selected'; }

$EmpNum = $_SESSION['EmpNum'];

echo "<span id='EmpNum_Report' style='display:none;'> $EmpNum </span>";

?>
<div class="report_table_div">

    <div class="container">

        <div class="Col-1-50" style="display:flex;">
            <span class='month_picker_label'>
                <div class="month">
                    <select id="month_picker">
                        <option id="all" value="ALL">ALL</option>
                        <option id="jan" value="01" <?php echo $selected_01 ?>>JANUARY</option>
                        <option id="feb" value="02" <?php echo $selected_02 ?>>FEBRUARY</option>
                        <option id="mar" value="03" <?php echo $selected_03 ?>>MARCH</option>
                        <option id="apr" value="04" <?php echo $selected_04 ?>>APRIL</option>
                        <option id="may" value="05" <?php echo $selected_05 ?>>MAY</option>
                        <option id="jun" value="06" <?php echo $selected_06 ?>>JUNE</option>
                        <option id="jul" value="07" <?php echo $selected_07 ?>>JULY</option>
                        <option id="aug" value="08" <?php echo $selected_08 ?>>AUGUST</option>
                        <option id="sep" value="09" <?php echo $selected_09 ?>>SEPTEMBER</option>
                        <option id="oct" value="10" <?php echo $selected_10 ?>>OCTOBER</option>
                        <option id="nov" value="11" <?php echo $selected_11 ?>>NOVEMBER</option>
                        <option id="dec" value="12" <?php echo $selected_12 ?>>DECEMBER</option>
                    </select>
                </div>
            </span>

            <div class="ui icon input">
                <input id="search" type="text" placeholder="Search..." value=''>
                <i class="search icon"></i>
            </div>

        </div>

        <div class="Col-2-50"></div>
        <button id="mobile_format" class="card_view"><i class="fa-solid fa-clone"></i> CARD FORMAT</button>

    </div>

    <div class="container" style="margin-top:10px; display:none;">
        <span id="jan" class="ui label month_text">JANUARY</span>
        <span id="feb" class="ui label month_text">FEBRUARY</span>
        <span id="mar" class="ui label month_text">MARCH</span>
        <span id="apr" class="ui label month_text">APRIL</span>
        <span id="may" class="ui label month_text">MAY</span>
        <span id="jun" class="ui label month_text">JUNE</span>
        <span id="jul" class="ui label month_text">JULY</span>
        <span id="aug" class="ui label month_text">AUGUST</span>
        <span id="sep" class="ui label month_text">SEPTEMBER</span>
        <span id="oct" class="ui label month_text">OCTOBER</span>
        <span id="nov" class="ui label month_text">NOVEMBER</span>
        <span id="dec" class="ui label month_text">DECEMBER</span>
    </div>

    <div id="ajax_month_report"></div>
    <div id="ajax_search_report"></div>
    <div id="ajax_report_table"></div>
</div>

<!-- SCRIPT FOR SEARCH -->
<script>

    var urlReport = "../Ajax_Report/report.php"
    var urlReport_Table = "../Ajax_Report/report_table.php"
    var ajax_report_table = "../Ajax_Report/ajax_report_table.php";
    var search = '';

    var date = new Date();
    var month = date.getMonth()+1;

    $(document).ready(function(){
        $('#ajax_report_table').load(ajax_report_table);
    });


    $("#search").on("input", function() {
        search = jQuery(this).val(); 
    });

    $('#month_picker').on('change', function() {
        month = this.value;

        console.log(search + " " + month);
    });

    $("#search").on("input", function() {

        search = jQuery(this).val(); //or this.value

        $('#ajax_report_table').empty();

        $.ajax({
            type: 'POST',
            url: '../Ajax_Report/ajax_search_report.php',
            data: {
                search: search,
                month: month

            },
            cache: false,
            success: function(data) {
                // alert(data);
                // $('.ajax_report').empty(urlReport);
                $('#ajax_search_report').html(data);
            },
            error: function(xhr, status, error) {
                console.error(xhr);
            }
        });

    });

    $('#month_picker').on('change', function() {

        $('#ajax_report_table').empty();

        $.ajax({
            type: 'POST',
            url: '../Ajax_Report/ajax_search_report.php',
            data: {
                month: month,
                search: search

            },
            cache: false,
            success: function(data) {
                $('#ajax_search_report').html(data);
            },
            error: function(xhr, status, error) {
                console.error(xhr);
            }

        });

    });

    $("#mobile_format").click(function(){

        if($("#mobile_format").hasClass("report_download_press")){

            $(".ajax_report").load(urlReport_Table);
            $(".ajax_report").empty(urlReport);

            $("#mobile_format").removeClass("report_download_press");

        }

        else{
            $(".ajax_report").empty(urlReport_Table);
            $(".ajax_report").load(urlReport);

        
            $("#mobile_format").addClass("report_download_press");

            $(".ajax_report").css({"display": "flex"});

        }

    });

</script>