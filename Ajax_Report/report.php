<?php

include '../1Connection.php';
include '../main_function/login.php';

$Admin = $_SESSION['Admin'];

echo "<span id='admin' style='display:none;'> $Admin </span>";

$currentYear = date('Y');
$currentMonth = date('m');
$currentDay = date('d');
$EndDayOfTheMonth = '';
$MonthName = date("F", mktime(0, 0, 0, $currentMonth, 10));

// $currentDate = $currentYear ."-". $currentMonth ."-". $currentDay;

?>

<div class="container" style="margin-top:30px; margin-right:30px;">
    <div class="ui icon input">
        <input id="search" type="text" placeholder="Search...">
        <i class="search icon"></i>
    </div>
</div>

<!-- <div class="container">
    <div class='close' id="ajax_search_mobile"></div>
</div> -->


<div class='close' id="ajax_search_mobile"></div>


<?php

// $query = "SELECT * FROM [dbo].[Receive] WHERE ARCHIVE = '0' ORDER BY DATE_RECEIVE DESC, id DESC";
// $result = sqlsrv_query($conn, $query);

$query = "SELECT * FROM [dbo].[Receive] 
WHERE ARCHIVE = '0'
AND INVOICE != 'STARTING' 
AND STATUS = 'RAW'
ORDER BY DATE_RECEIVE DESC, id DESC";
$result = sqlsrv_query($conn, $query);

while($rows=sqlsrv_fetch_array($result)){

    if( $rows['DATE_RECEIVE']->format('m') == "01" ){
        $card_bg = "#0000FF";
    }
    elseif( $rows['DATE_RECEIVE']->format('m') == "02" ){
        $card_bg = "#8F00FF";
    }
    elseif( $rows['DATE_RECEIVE']->format('m') == "03" ){
        $card_bg = "#F47F39";
        $color = "#fff";
    }
    elseif( $rows['DATE_RECEIVE']->format('m') == "04" ){
        $card_bg = "#A2B2AC";
        $color = "#fff";
    }
    elseif( $rows['DATE_RECEIVE']->format('m') == "05" ){
        $card_bg = "#4C9A2A";
    }
    elseif( $rows['DATE_RECEIVE']->format('m') == "06" ){
        $card_bg = "#0D0C12";
        $color = "#fff";
    }
    elseif( $rows['DATE_RECEIVE']->format('m') == "07" ){
        $card_bg = "#FFC0CB";
    }
    elseif( $rows['DATE_RECEIVE']->format('m') == "08" ){
        $card_bg = "#964B00";
    }
    elseif( $rows['DATE_RECEIVE']->format('m') == "09" ){
        $card_bg = "#FED758";
    }
    elseif( $rows['DATE_RECEIVE']->format('m') == "10" ){
        $card_bg = "#7AD7F0";
    }
    elseif( $rows['DATE_RECEIVE']->format('m') == "11" ){
        $card_bg = "#FFFDFA";
    }
    else{
        $card_bg = "#8e2828";
    }

    // IF PARA ITAGO YUNG EDIT AT DELETE BUTTON KAPAG LAGPAS NA SA DATE KUNG KELAN NILA NA RECEIVE OR ENCODE
    // if($rows['DATE_RECEIVE']->format('Y-m-d') < $currentDate){
    //     echo "<script>
    //             $('#editid".$rows['id']."').addClass('close');
    //             $('#deleteid".$rows['id']."').addClass('close');
    //           </script>";
    // }


    if($rows['STATUS'] != 'RAW'){
        echo "<script>
                $('#editid".$rows['id']."').addClass('close');
                $('#deleteid".$rows['id']."').addClass('close');
              </script>";
        $status = "green";
    }
    else{
        $status = "#880808";
    }


    echo "<div class='report_card default' style='border: 10px solid".$card_bg.";'>
            
            <table>
                <tr class='close'>
                    <td class='transac_id'>Transaction ID: </td>
                    <td class='transac_id'> " . $rows['id'] . "</td>
                </tr>
                <tr class='close'>
                    <td class='report_label'> Status: </td>
                    <td style='font-weight:700; color:".$status.";' class='report_value'>". $rows['STATUS'] . "</td>
                </tr>
                <tr>
                    <td class='report_label'> Date Receive: </td>
                    <td class='report_value'> ". $rows['DATE_RECEIVE']->format('Y-m-d') . "</td>
                </tr>
                <tr>
                    <td class='report_label'> Goods Code: </td>
                    <td style='font-weight:700;' id='goodscode".$rows['id']."' class='report_value'>". $rows['GOODS_CODE'] . "</td>
                </tr>
                <tr>
                    <td class='report_label'> Invoice No.: </td>
                    <td style='font-weight:700;' class='report_value'> ". $rows['INVOICE'] . "</td>
                </tr>
                <tr>
                    <td class='report_label'> Quantity: </td>
                    <td style='font-weight:700;' class='report_value'>". number_format($rows['QTY']) . "</td>
                </tr>
                <tr>
                    <td class='report_label'> Assy Line: </td>
                    <td class='report_value'> ". $rows['ASY_LINE'] . "</td>
                </tr>
                <tr>
                    <td class='report_label'> Supplier: </td>
                    <td class='report_value'> ". $rows['SUPPLIER'] . "</td>
                </tr>
                
                <tr>
                    <td class='report_label'> Materials Type: </td>
                    <td class='report_value'> ". $rows['MATERIALS_TYPE'] . "</td>
                </tr>
                <tr>
                    <td class='report_label'> Item Code: </td>
                    <td id='itemcode".$rows['id']."' class='report_value'>". $rows['ITEM_CODE'] . "</td>
                </tr>
                
                
                <tr>
                    <td class='report_label'> Purchase Slip: </td>
                    <td class='report_value'> ". $rows['P_SLIP'] . "</td>
                </tr>
                <tr>
                    <td class='report_label'> Sales Slip: </td>
                    <td class='report_value'> ". $rows['S_SLIP'] . "</td>
                </tr>
                <tr>
                    <td class='report_label'> Part Number: </td>
                    <td class='report_value'> ". $rows['PART_NUMBER'] . "</td>
                </tr>
                <tr>
                    <td class='report_label'> Part Name: </td>
                    <td class='report_value'> ". $rows['PART_NAME'] . "</td>
                </tr>
                <tr>
                    <td style='text-align:center;'> <button id='editid".$rows['id']."' class='edit'> <i class='fa-solid fa-file-pen'></i> Edit</button> </td>
                    <td style='text-align:center;'> <button id='deleteid".$rows['id']."' class='delete'> <i class='fa-solid fa-delete-left'></i> Delete</button> </td>
                </tr>
            </table>

          </div>

          <script>

            $('#deleteid".$rows['id']."').click(function(){

                var id_num = ".$rows['id'].";
                var prev_qty = ".$rows['QTY'].";

                var goods = document.getElementById('goodscode".$rows['id']."');
                var goods_value = goods.textContent;

                var item = document.getElementById('itemcode".$rows['id']."');
                var item_value = item.textContent;

                var urlReport = '../Ajax_Report/report.php';

                $.ajax({
                    type: 'POST',
                    url: '../Ajax_Report/delete_report.php',
                    data: {
                        id_num: id_num,
                        prev_qty: prev_qty,
                        goods_value: goods_value,
                        item_value: item_value
    
                    },
                    cache: false,
                    success: function(data) {
                        alert(data);
                        
                        $('.ajax_report').load(urlReport);
                    },
                    error: function(xhr, status, error) {
                        console.error(xhr);
                    }
    
                });
            });

            $('#editid".$rows['id']."').click(function(){
                
                var id_num = ".$rows['id'].";

                var urlReport = '../Ajax_Report/report.php';

                var urlEdit = '../Ajax_Report/edit_report.php';

                $.ajax({
                    type: 'POST',
                    url: '../Ajax_Report/edit_report.php',
                    data: {
                        id_num: id_num
    
                    },
                    cache: false,
                    success: function(data) {
                        $('.ajax_report').empty(urlReport);
                        // alert(data);
                        $('.ajax_report').html(data);
                    },
                    error: function(xhr, status, error) {
                        console.error(xhr);
                    }
    
                });
            });
          </script>";

        
        // else{
        //     echo "<script>
        //             $('.edit').removeClass('close');
        //           </script>";
        // }

}

?>



<script>
    var Admin = document.getElementById('admin');
    var Admin_value = Admin.textContent;

    if( Admin_value == 0 ){

        $(".delete").addClass("close");
        
    }
    else if( Admin_value == 2 ){

        $(".delete").addClass("close");
        $(".edit").addClass("close");

    }
    
</script>

<script>

    var search = '';

    $("#search").on("input", function() {

        search = jQuery(this).val(); //or this.value
        console.log(search);

        if(search == ''){
            $('.default').removeClass('close');
            $('#ajax_search_mobile').addClass('close');
            $('#ajax_search_mobile').empty();
            // $('#ajax_search_report').addClass('close');
            // $('#ajax_month_report').addClass('close');
        }
        else{
            $('.default').addClass('close');
            $('#ajax_search_mobile').removeClass('close');
            // $('#ajax_month_report').addClass('close');
            // $('#ajax_search_report').removeClass('close');
            // $('#ajax_month_report').addClass('close');

            $.ajax({
                type: 'POST',
                url: '../Ajax_Report/ajax_search_mobile.php',
                data: {
                    search: search

                },
                cache: false,
                success: function(data) {
                    // alert(data);
                    // $('.ajax_report').empty();
                    // $('.ajax_report').html(data);
                    $('#ajax_search_mobile').html(data);
                },
                error: function(xhr, status, error) {
                    console.error(xhr);
                }
            });
        }

    });

</script>
