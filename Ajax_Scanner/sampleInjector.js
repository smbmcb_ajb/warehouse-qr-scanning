// QR Code generator
var boxDetailChanger = ()=>{
    // document.getElementById("boxDetails-container").textContent = document.getElementById("boxNumber").value + "/" + document.getElementById("totalNumberOfBox").value
}
export var generateQR = ()=>{
        if(document.getElementById('invoice_input').value == "" || document.getElementById('invoice_input').value == null){
            return Swal.fire({
            icon: 'error',
            title: 'Oops...',
            text: 'Invoice number is required!',
})
        }
        document.getElementById("invoice-container").textContent = ""
        document.getElementById("itemCode-container").textContent = ""
        document.getElementById("color-container").textContent = ""
        document.getElementById("goodsCode-container").textContent = ""
        document.getElementById("boxDetails-container").textContent = ""
        // document.getElementById("boxNumber").value = ""
        document.getElementById("totalNumberOfBox").value = ""
        // document.getElementById("numberOfItems").value = ""

        document.getElementById("invoice-container").textContent = document.getElementById('invoice_input').value
        document.getElementById("itemCode-container").textContent = document.getElementById('itemCode').value
        
        // document.getElementById("quantityContainer").value = document.getElementById('quantity_input').value
        if(document.getElementById('dateReceive').value == '01'){
            document.getElementById("color-container").style.backgroundColor = "#0000FF";
                }
                else if(document.getElementById('dateReceive').value == '02'){
                    document.getElementById("color-container").style.backgroundColor = "#8F00FF";
                }
                else if(document.getElementById('dateReceive').value == '03'){
                    document.getElementById("color-container").style.backgroundColor = "#F47F39";
                }
                else if(document.getElementById('dateReceive').value == '04'){
                    document.getElementById("color-container").style.backgroundColor = "#A2B2AC";
                    
                }
                else if(document.getElementById('dateReceive').value == '05'){
                    document.getElementById("color-container").style.backgroundColor = "#4C9A2A";
                }
                else if(document.getElementById('dateReceive').value == '06'){
                    document.getElementById("color-container").style.backgroundColor = "#0D0C12";
                }
                else if(document.getElementById('dateReceive').value == '07'){
                    document.getElementById("color-container").style.backgroundColor = "#FFC0CB";
                    
                }
                else if(document.getElementById('dateReceive').value == '08'){
                    document.getElementById("color-container").style.backgroundColor = "#964B00";
                }
                else if(document.getElementById('dateReceive').value == '09'){
                    document.getElementById("color-container").style.backgroundColor = "#FED758";
                    
                }
                else if(document.getElementById('dateReceive').value == '10'){
                    document.getElementById("color-container").style.backgroundColor = "#7AD7F0";
                    
                }
                else if(document.getElementById('dateReceive').value == '11'){
                    document.getElementById("color-container").style.backgroundColor = "#FFFDFA";
                    
                }
                else{
                    document.getElementById("color-container").style.backgroundColor = "#880808";
                }
        document.getElementById("color-container").textContent = ""
        document.getElementById("goodsCode-container").textContent = document.getElementById('goods_code').textContent
    
        
        document.getElementById("qr-code").innerHTML = ""
        // document.getElementById("barcode").innerHTML = ""
        var qrcode = new QRCode("qr-code", {
                    width: 180,
                    height: 180,
                    correctLevel : QRCode.CorrectLevel.H
                });
                
        qrcode.makeCode(document.getElementById('invoice_input').value+";"+document.getElementById('goods_code').textContent+";"+document.getElementById('itemCode').value);
        
        // if(document.getElementById('itemCode').value){
        //     JsBarcode("#barcode", document.getElementById('itemCode').value,{
        //     background: "#E6E6E6",
        //     height:50
        // });
        // }
        
        // or with jQuery
        // $("#barcode").JsBarcode("Hi!");
        $('#staticBackdrop').modal('show')
       
    }

    // generateQR()
    // QR Code generator