<?php 

include '../1Connection.php';

$Code = $_POST['Code'] ?? '';
$Assy = $_POST['Assy'] ?? '';
$Po = $_POST['Po'] ?? '';
$MS_Po = $_POST['MS_Po'] ?? '';
$D_Date = $_POST['D_Date'] ?? '';

$currentYear = date('Y');
$currentMonth = date('m');
$currentDay = date('d');
$EndDayOfTheMonth = '';

$MonthName = date("F", mktime(0, 0, 0, $currentMonth, 10));

// ORIGINAL QUERY
// $query = "SELECT * FROM [dbo].[Masterlist] 
// WHERE PRODUCT_COAT = '$Code'
// OR  PRODUCT_KEY = '$Code'
// OR ABOLITION = '$Code'
// OR PRODUCT_NAME = '$Code'";
// $result = sqlsrv_query($conn, $query);

// $query = "SELECT * FROM [dbo].[Masterlist] 
// WHERE CHARINDEX('$Code', PRODUCT_NAME) > 0
// OR CHARINDEX('$Code', PRODUCT_COAT) > 0";
// $result = sqlsrv_query($conn, $query);

$query = "SELECT * FROM MS21_MASTER_LIST
WHERE CHARINDEX('$Code', UPDATED_PRODUCT_NAME) > 0
OR CHARINDEX('$Code', GOODS_CODE) > 0
AND CHARINDEX('$Assy', ASSY_LINE) > 0";
$result = sqlsrv_query($connTesting, $query);

$display_count = 0;

while($rows=sqlsrv_fetch_array($result)){

    

?>

<div>

    <div class="receive_div margin_bottom">

        <div class="margin_bottom">
            <span class='receive_label' id="date"> <?php echo $MonthName . " " . $currentDay . ", " . $currentYear; ?> </span>
        </div>

        <div class="CONTAINER margin_bottom">

            <!-- GOODS CODE -->
            <div class="container flex">

                <div class="col-1-35">
                    <div class='receive_label container margin_bottom'> Goods Code: </div>
                    <!-- <div class='receive_label container margin_bottom'> Supplier: </div>
                    <div class='receive_label container margin_bottom'> Item Code: </div>
                    <div class='receive_label container margin_bottom close'> Materials Type: </div>
                    <div class='receive_label container margin_bottom'> Part Number: </div>
                    <div class='receive_label container margin_bottom'> Part Name: </div>
                    <div class='receive_label container margin_bottom'> TOSS P.O: </div>
                    <div class='receive_label container margin_bottom'> MS21 P.O: </div>
                    <div class='receive_label container margin_bottom'> Assy Line: </div>
                    <div class='receive_label container margin_bottom'> Quantity: </div>
                    <div class='receive_label container margin_bottom'> Invoice No: </div> -->
                </div>

                <div class="col-1-65">
                    <?php 
                        echo "<span class='receive_content container margin_bottom' id='goods_code'>" . $rows['GOODS_CODE'] . "</span>";
                        // echo "<span class='receive_content container margin_bottom' id='supplier'>" . $rows['MANUFACTURER_NAME'] . "</span>";
                        // echo "<span class='receive_content container margin_bottom' id='item_code'>" . $rows['ITEM_CODE'] . "</span>";
                        // echo "<span class='receive_content container margin_bottom close' id='material'>" . $rows['FD_NAME'] . "</span>";
                        // echo "<span class='receive_content container margin_bottom' id='part_num'>".$rows['PART_NUMBER']."</span>";
                        // echo "<span class='receive_content container margin_bottom' id='part_name'>".$rows['FD_NAME']."</span>";
                        // echo "<span class='receive_content container margin_bottom' id='PO_input'>".$Po."</span>";
                        // echo "<span class='receive_content container margin_bottom' id='PO_input'>".$MS_Po."</span>";
                        // echo "<span class='receive_content container margin_bottom' id='assy_line'>".$rows['ASSY_LINE']."</span>";
                    ?>
                    <!-- <input type="number" class="receive_input" id="quantity_input" value="">
                    <input type="text" class="receive_input" id="invoice_input" value=""> -->
                </div>

            </div>

            <!-- SUPPLIER -->
            <div class="container flex">

                <div class="col-1-35">
                    <div class='receive_label container margin_bottom'> Supplier: </div>
                </div>

                <div class="col-1-65">
                    <?php 
                        echo "<span class='receive_content container margin_bottom' id='supplier'>" . $rows['MANUFACTURER_NAME'] . "</span>";
                    ?>
                </div>

            </div>

            <!-- ITEM CODE -->
            <div class="container flex">

                <div class="col-1-35">
                    <div class='receive_label container margin_bottom'> Item Code: </div>
                </div>

                <div class="col-1-65">
                    <?php 
                        echo "<span class='receive_content container margin_bottom' id='item_code'>" . $rows['ITEM_CODE'] . "</span>";
                    ?>
                </div>

            </div>

            <!-- PART NUMBER -->
            <div class="container flex">

                <div class="col-1-35">
                    <div class='receive_label container margin_bottom'> Part Number: </div>
                </div>

                <div class="col-1-65">
                    <?php 
                        echo "<span class='receive_content container margin_bottom' id='part_num'>" . $rows['PART_NUMBER'] . "</span>";
                    ?>
                </div>

            </div>

            <!-- PART NAME -->
            <div class="container flex">

                <div class="col-1-35">
                    <div class='receive_label container margin_bottom'> Part Name: </div>
                </div>

                <div class="col-1-65">
                    <?php 
                        echo "<span class='receive_content container margin_bottom' id='part_name'>" . $rows['FD_NAME'] . "</span>";
                    ?>
                </div>

            </div>

            <!-- TOSS P.O -->
            <div class="container flex">

                <div class="col-1-35">
                    <div class='receive_label container margin_bottom'> TOSS P.O: </div>
                </div>

                <div class="col-1-65">
                    <?php 
                        echo "<span class='receive_content container margin_bottom' id='PO_input'>" . $Po . "</span>";
                    ?>
                </div>

            </div>

            <!-- MS21 P.O -->
            <div class="container flex">

                <div class="col-1-35">
                    <div class='receive_label container margin_bottom'> MS21 P.O: </div>
                </div>

                <div class="col-1-65">
                    <?php 
                        echo "<span class='receive_content container margin_bottom'>" . $MS_Po . "</span>";
                    ?>
                </div>

            </div>

            <!-- DELIVERY DATE -->
            <div class="container flex">

                <div class="col-1-35">
                    <div class='receive_label container margin_bottom'> P.O Delivery Date: </div>
                </div>

                <div class="col-1-65">
                    <?php 
                        echo "<span class='receive_content container margin_bottom'>" . $D_Date . "</span>";
                    ?>
                </div>

            </div>

            <!-- ASSY LINE -->
            <div class="container flex">

                <div class="col-1-35">
                    <div class='receive_label container margin_bottom'> Assy Line: </div>
                </div>

                <div class="col-1-65">
                    <?php 
                        echo "<span class='receive_content container margin_bottom' id='assy_line'>" . $rows['ASSY_LINE'] . "</span>";
                    ?>
                </div>

            </div>

            <!-- QUANTITY -->
            <div class="container flex">

                <div class="col-1-35">
                    <div class='receive_label container margin_bottom'> Quantity: </div>
                </div>

                <div class="col-1-65">
                    <input type="number" class="receive_input" id="quantity_input" value="">
                </div>

            </div>

            <!-- INVOICE -->
            <div class="container flex">

                <div class="col-1-35">
                    <div class='receive_label container margin_bottom'> Invoice No: </div>
                </div>

                <div class="col-1-65">
                    <input type="text" class="receive_input" id="invoice_input" value="">
                </div>

            </div>

            <!-- Total Nnumber of Boxes -->
            <div class="container flex">

                <div class="col-1-35">
                    <div class='receive_label container margin_bottom'> Total Number of Boxes: </div>
                </div>

                <div class="col-1-65">
                    <input type="text" class="receive_input" id="totalNumberOfBox_input" value="">
                </div>

            </div>
        </div>
        <!-- <div>sdfsdfdsfdfs</div> -->
        
        <button class="receive_button" id="receive_btn">RECEIVE</button>
        <!-- <button class="receive_button mt-2" id="printQR_btn" onclick="generateQR()">Generate QR</button> -->

    </div>

    <div class="receive_div close">

        <div class="margin_bottom close">
            <span class='receive_label'> Purchase Slip: </span> <br>
            <input type="text" class="receive_input" id="purchase_slip" value="IM<?php echo $currentYear . $currentMonth ?>-000">
        </div>

        <div class="margin_bottom close">
            <span class='receive_label'> Sales Slip: </span> <br>
            <input type="text" class="receive_input" id="sales_slip" value="INV<?php echo $currentYear . $currentMonth ?>-000"> 
        </div>

        <div class="margin_bottom">
            
        </div>

    </div>

</div>

<!-- QR Code generator -->
<?php } ?>

    <?php 
    // echo $Po;
    // echo $Assy;
    // echo $Code;
        $sql = "SELECT * FROM [Receive]
        WHERE PO = '$Po' 
        AND GOODS_CODE = '$Code' 
        AND ASY_LINE = '$Assy'
        ";
        $resulta = sqlsrv_query($conn, $sql);

        if($resulta === false) {
            die( print_r( sqlsrv_errors(), true) );
            }
        while($row = sqlsrv_fetch_array($resulta)){
            echo "<input type='hidden' id='dateReceive' value='".$row['DATE_RECEIVE']->format('m')."'>";
            echo "<input type='hidden' id='itemCode' value='".$row['ITEM_CODE']."'>";
            // list($day, $month, $year) = explode("/", date('d/m/Y/'));
            // echo $month . '/' . $day . '/' . $year;
            // echo "<br>";
            $capitalMonth = $row['DATE_RECEIVE']->format('M');
            $capitalMonth = strtoupper($capitalMonth);
            
            echo "<input type='hidden' id='ReceivedDate' value='".$capitalMonth.$row['DATE_RECEIVE']->format(' d, Y')."'>";
            // echo $row['DATE_RECEIVE']->format('M');
            // echo $row['DATE_RECEIVE']->date;
            // 
        }
    ?>
    <?php
    $sqlItemcode = "SELECT * FROM [WH_ITEMCODE] WHERE GOODS_CODE = '$Code'";
        $resultItem = sqlsrv_query($conn_Itemcode, $sqlItemcode);

        if($resultItem === false) {
            die( print_r( sqlsrv_errors(), true) );
            }
        while($rowItem = sqlsrv_fetch_array($resultItem, SQLSRV_FETCH_ASSOC)){
            // echo $row['ITEM_CODE'];
            $wh_item_code = $rowItem['ITEM_CODE'];
            // echo "<input type='text' id='itemCodeWh' value='".$rowItem['ITEM_CODE']."'>";
        }
        
    ?>
<!-- QR Code generator -->    

<script> //AJAX PARA SA SEPARATE NA FILE NG PAG CONFIRM SA 7 AM
    
    var dtToday = new Date();

    var month = dtToday.getMonth() + 1;
    var day = dtToday.getDate();
    var year = dtToday.getFullYear();
                
    var CurrentDate = year + '-' + month + '-' + day;

    // jQuery('#quantity_input').blur(function () {
    //     var Name = jQuery(this).val(); //or this.value
    //     console.log(Name);
    // });

    var quantity = '';
    var invoice = '';
    var totalNumberOfBox = '';
    // var po = $("#PO_input").val();

    jQuery('#quantity_input').blur(function () {
        quantity = jQuery(this).val(); //or this.value
        // console.log(quantity);
    });

    jQuery('#invoice_input').blur(function () {
        invoice = jQuery(this).val(); //or this.value
        // console.log(invoice);
    });
    jQuery('#totalNumberOfBox_input').blur(function () {
        totalNumberOfBox = jQuery(this).val(); //or this.value
        // console.log(invoice);
    });

    // jQuery('#PO_input').blur(function () {
    //     po = jQuery(this).val(); //or this.value
    //     // console.log(invoice);
    // });AL_VALUE
    
        
    $("#receive_btn").click(function() {

        // console.log("sdfasdfasdf")
        var goods_code = document.getElementById('goods_code');
        var goods_code_value = goods_code.textContent;
        // console.log(goods_code_value);

        var supplier = document.getElementById('supplier');
        var supplier_value = supplier.textContent;
        // console.log(supplier_value);

        var item_code = document.getElementById('item_code');
        var item_code_value = item_code.textContent;
        // console.log(item_code_value);

        // var material = document.getElementById('material');
        // var material_value = material.textContent;
        // console.log(material_value);

        var part_num = document.getElementById('part_num');
        var part_num_value = part_num.textContent;
        // console.log(part_num_value);

        var part_name = document.getElementById('part_name');
        var part_name_value = part_name.textContent;
        // console.log(part_name_value);

        // var location = document.getElementById('location');
        // var location_value = location.textContent;
        // console.log(location_value);

        var po_raw = document.getElementById('PO_input');
        var po = po_raw.textContent;
        // console.log(po)

        var assy_line = document.getElementById("assy_line");
        var assy_line_value = assy_line.textContent;
        // console.log(assy_line_value);

        // var quantity = $("#quantity_input").attr('value');
        // console.log(quantity);

        // var invoice = $("#invoice_input").attr('value');
        // console.log(invoice);

        
        var purchase_slip = $("#purchase_slip").val();
        // console.log(purchase_slip);

        var sales_slip = $("#sales_slip").val();
        console.log(sales_slip);

        $.ajax({
            type: "POST",
            url: '../Ajax_Scanner/upload_receive.php',
            data: {
                goods_code_value: goods_code_value,
                supplier_value: supplier_value,
                item_code_value: item_code_value,
                // material_value: material_value,
                part_num_value: part_num_value,
                part_name_value: part_name_value,
                assy_line_value: assy_line_value,
                quantity: quantity,
                invoice: invoice,
                purchase_slip: purchase_slip,
                sales_slip: sales_slip,
                // location_value: location_value,
                po: po,
                totalNumberOfBox: totalNumberOfBox

            },
            cache: false,
            success: function(data) {
                // alert(data);
                $("#QR-Code").val('');
                $("#quantity_input").val('');
                $("#invoice_input").val('');
                $("#totalNumberOfBox_input").val('');

                $(".ajax_table_desktop").html(data);
            },
            error: function(xhr, status, error) {
                console.error(xhr);
            }

        });

        // document.getElementById('testinput').value = document.getElementById('goods_code').textContent

        // generateQR()
        // document.getElementById('totalNumberOfBox').focus();
        
    });
      
</script>

<script>
    // QR Code generator
    
    console.log('<?php echo $Po; ?>')
    var boxDetailChanger = ()=>{
        if(isNaN(document.getElementById("totalNumberOfBox").value)){
            document.getElementById("error-container").style.display = "flex"
            document.getElementById("error-container").textContent = "Please input a valid number!"
            document.getElementById("btnPrint").disabled = true
        } else {
            document.getElementById("error-container").style.display = "none"
            document.getElementById("btnPrint").disabled = false
        }
        if(document.getElementById("totalNumberOfBox").value == "" || document.getElementById("totalNumberOfBox").value == null){
            document.getElementById("btnPrint").disabled = true
        }
        document.getElementById("totalBox").textContent = document.getElementById("totalNumberOfBox").value
    }
    var generateQR = ()=>{
            if(document.getElementById('invoice_input').value == "" || document.getElementById('invoice_input').value == null){
                return Swal.fire({
                icon: 'error',
                title: 'Oops...',
                text: 'Invoice number is required!',
                })
            }
            document.getElementById("invoice-container").textContent = ""
            // document.getElementById("itemCode-container").textContent = ""
            // document.getElementById("color-container").textContent = ""
            // document.getElementById("goodsCode-container").textContent = ""
            // document.getElementById("boxDetails-container").textContent = ""
            // document.getElementById("boxNumber").value = ""
            document.getElementById("totalNumberOfBox").value = ""
            // document.getElementById("numberOfItems").value = ""

            document.getElementById("invoice-container").textContent = document.getElementById('invoice_input').value
            // document.getElementById("itemCode-container").textContent = document.getElementById('itemCode').value
            
            document.getElementById('dateReceived').innerText = document.getElementById('ReceivedDate').value
            // document.getElementById("quantityContainer").value = document.getElementById('quantity_input').value

            // document.getElementById('commodityCode').innerText = document.getElementById('itemCodeWh').value
            <?php
                if(isset($wh_item_code)){
                    ?>
                    document.getElementById('commodityCode').innerText = '<?php echo $wh_item_code; ?>'
                    <?php
                    echo "console.log('tested')";
                }
            ?>
            // 

            if(document.getElementById('dateReceive').value == '01'){
                document.getElementById("color-container").style.backgroundColor = "#0000FF";
                document.getElementById("color-container").style.color = "white";
                    }
                    else if(document.getElementById('dateReceive').value == '02'){
                        document.getElementById("color-container").style.backgroundColor = "#8F00FF";
                        document.getElementById("color-container").style.color = "white";
                    }
                    else if(document.getElementById('dateReceive').value == '03'){
                        document.getElementById("color-container").style.backgroundColor = "#F47F39";
                    }
                    else if(document.getElementById('dateReceive').value == '04'){
                        document.getElementById("color-container").style.backgroundColor = "#A2B2AC";
                        
                    }
                    else if(document.getElementById('dateReceive').value == '05'){
                        document.getElementById("color-container").style.backgroundColor = "#4C9A2A";
                        document.getElementById("color-container").style.color = "white";
                    }
                    else if(document.getElementById('dateReceive').value == '06'){
                        document.getElementById("color-container").style.backgroundColor = "#0D0C12";
                        document.getElementById("color-container").style.color = "white";
                    }
                    else if(document.getElementById('dateReceive').value == '07'){
                        document.getElementById("color-container").style.backgroundColor = "#FFC0CB";
                        
                    }
                    else if(document.getElementById('dateReceive').value == '08'){
                        document.getElementById("color-container").style.backgroundColor = "#964B00";
                        document.getElementById("color-container").style.color = "white";
                    }
                    else if(document.getElementById('dateReceive').value == '09'){
                        document.getElementById("color-container").style.backgroundColor = "#FED758";
                        
                    }
                    else if(document.getElementById('dateReceive').value == '10'){
                        document.getElementById("color-container").style.backgroundColor = "#7AD7F0";
                        
                    }
                    else if(document.getElementById('dateReceive').value == '11'){
                        document.getElementById("color-container").style.backgroundColor = "#FFFDFA";
                        
                    }
                    else{
                        document.getElementById("color-container").style.backgroundColor = "#880808";
                        document.getElementById("color-container").style.color = "white";
                    }
            // document.getElementById("color-container").textContent = ""
            // document.getElementById("goodsCode-container").textContent = document.getElementById('goods_code').textContent
        
            
            document.getElementById("qrcode").innerHTML = ""
            // document.getElementById("barcode").innerHTML = ""
            setTimeout(()=>{
                var qrcode = new QRCode("qrcode", {
                        width: 140,
                        height: 130
                        // correctLevel : QRCode.CorrectLevel.H
                    });
                    qrcode.makeCode("<?php echo $Po.";".$Assy; ?>;"+document.getElementById('invoice_input').value+";"+document.getElementById('goods_code').textContent+";"+document.getElementById('itemCode').value+";"+document.getElementById('ReceivedDate').value);
            },1000)
            document.getElementById("qrcode-for-print").innerHTML = ""
            setTimeout(()=>{
                var qrcode = new QRCode("qrcode-for-print", {
                        width: 120,
                        height: 100
                        // correctLevel : QRCode.CorrectLevel.H
                    });
                    qrcode.makeCode("<?php echo $Po.";".$Assy; ?>;"+document.getElementById('invoice_input').value+";"+document.getElementById('goods_code').textContent+";"+document.getElementById('itemCode').value+";"+document.getElementById('ReceivedDate').value);
            },1000)
                    
            
            
            // if(document.getElementById('itemCode').value){
            //     JsBarcode("#barcode", document.getElementById('itemCode').value,{
            //     background: "#E6E6E6",
            //     height:50
            // });
            // }
            
            // or with jQuery
            // $("#barcode").JsBarcode("Hi!");
            $('#staticBackdrop').modal('show')
        }

        // generateQR()
        // QR Code generator
</script>