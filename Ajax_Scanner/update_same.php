<?php

include '../1Connection.php';

$currentYear = date('Y');
$currentMonth = date('m');
$currentDay = date('d');

$currentDate = $currentYear . "-" . $currentMonth . "-" . $currentDay;

$goods_code = $_POST['goods_code'] ?? '';
$item_code = $_POST['item_code'] ?? '';
$invoice = $_POST['invoice'] ?? '';
$quantity = $_POST['quantity'] ?? '';
$totalNumberOfBox = $_POST['totalNumberOfBox'];
// $part_num_value = $_POST['part_num_value'] ?? '';
// $part_name_value = $_POST['part_name_value'] ?? '';
// $assy_line_value = $_POST['assy_line_value'] ?? '';
// $quantity = $_POST['quantity'] ?? '';
// $invoice = $_POST['invoice'] ?? '';
// $purchase_slip = $_POST['purchase_slip'] ?? '';
// $sales_slip = $_POST['sales_slip'] ?? '';
// $location_value = $_POST['location_value'] ?? '';
$archieve = 0;

$query1 = "SELECT * FROM [dbo].[Receiving] 
WHERE GOODS_CODE = '$goods_code'
AND ITEM_CODE ='$item_code'
AND INVOICE = '$invoice'";
$result = sqlsrv_query($conn, $query1);

while($row=sqlsrv_fetch_array($result)){

    $part_num = $row['PART_NUMBER'];
    $part_name = $row['PART_NAME'];
    $assy_line = $row['ASY_LINE'];
    $supplier = $row['SUPPLIER'];
    $materials = $row['MATERIALS_TYPE'];

}

// echo $part_num . " ";
// echo $part_name . " ";
// echo $assy_line . " ";

// $NEW_QTY = $OLD_QTY + $quantity;
// $OLD_QTY_S = $OLD_QTY_S + $quantity;

// $tsql = "UPDATE [Receive] 
// SET QTY = '$NEW_QTY',
// ADD_COUNT = '$NEW_ADD_COUNT'
// WHERE GOODS_CODE = '$goods_code_value'
// AND ITEM_CODE ='$item_code_value'";
    
// $stmt2 = sqlsrv_query( $conn, $tsql);

$tsql = "INSERT INTO [Receiving] 
(GOODS_CODE, ASY_LINE, SUPPLIER, MATERIALS_TYPE, ITEM_CODE, PART_NUMBER, PART_NAME, QTY, DATE_RECEIVE, INVOICE, ARCHIVE, QTY_S, P_SLIP, STATUS, NUMBER_OF_BOX)
VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)";
                
$params1 = array(
    $goods_code, 
    $assy_line, 
    $supplier, 
    $materials, 
    $item_code, 
    $part_num, 
    $part_name, 
    $quantity, 
    $currentDate,
    $invoice,
    $archieve,
    $quantity,
    0,
    'RAW',
    $totalNumberOfBox);

$stmt1 = sqlsrv_query( $conn, $tsql, $params1);
    
if( $stmt1 ){
    // echo 'Upload was successful.';

    echo "<script language='javascript'>
            Swal.fire({
                icon: 'success',
                title: 'Received!',
                text: 'Upload was successful!',
            })
        </script>";

    // QUERY PARA SA PAG INSERT AT UPDATE NG TOTAL STOCK NG MATERIALS
    // $query2 = sqlsrv_query( $conn, "SELECT * FROM [Total_Stock] 
    // WHERE GOODS_CODE = '$goods_code'
    // AND ITEM_CODE ='$item_code'", array());

    // if ($query2 !== NULL) {  

    //     $rows = sqlsrv_has_rows( $query2 ); 
        
    //     if ($rows === true) {

    //         // QUERY PARA KUNIN YUNG TOTAL STOCKS NG EXISTING NA MATERIAL
    //         $query1 = "SELECT * FROM [dbo].[Total_Stock] 
    //         WHERE GOODS_CODE = '$goods_code'
    //         AND ITEM_CODE ='$item_code'";
    //         $result = sqlsrv_query($conn, $query1);

    //         while($row=sqlsrv_fetch_array($result)){

    //             $OLD_TOTAL_STOCK = $row['TOTAL_STOCK'];
    //             $ADD_COUNT = $row['ADD_COUNT'];

    //         }
    //         $NEW_TOTAL_STOCK = $OLD_TOTAL_STOCK + $quantity;
    //         $NEW_ADD_COUNT = $ADD_COUNT + 1;
            
    //         $tsql = "UPDATE [Total_Stock] 
    //         SET TOTAL_STOCK = '$NEW_TOTAL_STOCK',
    //         ADD_COUNT = '$NEW_ADD_COUNT'
    //         WHERE GOODS_CODE = '$goods_code'
    //         AND ITEM_CODE ='$item_code'";
                
    //         $stmt2 = sqlsrv_query( $conn, $tsql);
                        
    //         if( $stmt2 ){

    //             date_default_timezone_set("Asia/Hong_Kong");
    //             $current_date_with_time =  date("Y-m-d H:i:s");

    //             //QUERY PARA MAKAPAG LAGAY NG DATA SA STOCK CARD REPORT
    //             $tsqlStockCard = "INSERT INTO [transaction_record_tbl] 
    //             (TRANSACTION_DATE, GOODS_CODE, ITEM_CODE, QTY_ISSUED, TOTAL_STOCK, PART_NUMBER, PART_NAME, QTY_RECEIVED, INVOICE_KIT)
    //             VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?)";
                        
    //             $params1StockCard = array(
    //                 $current_date_with_time,
    //                 $goods_code, 
    //                 $item_code,
    //                 0,
    //                 $NEW_TOTAL_STOCK,
    //                 $part_num, 
    //                 $part_name, 
    //                 $quantity, 
    //                 $invoice);

    //             $stmt1StockCard = sqlsrv_query( $connStock, $tsqlStockCard, $params1StockCard);

    //             if( $stmt1StockCard ){

    //             }
                                                            
    //             else
    //             {
    //                 echo 'Error: The system is unable to update the quantity of stocks; thus, please contact the developer as soon as possible.';
    //                 die( print_r( sqlsrv_errors(), true));
    //             }

    //         }
                                                        
    //         else
    //         {
    //             echo 'Error: The system is unable to update the quantity of stocks; thus, please contact the developer as soon as possible.';
    //             die( print_r( sqlsrv_errors(), true));
    //         }

    //     }
    // }
}
else
{
    // echo 'The upload failed.';
    echo "<script language='javascript'>
            Swal.fire({
                icon: 'error',
                title: 'Error!',
                text: 'Dial the developer right now! ".print_r( sqlsrv_errors(), true)."',
            })
        </script>";
    // die( print_r( sqlsrv_errors(), true));
}

?>