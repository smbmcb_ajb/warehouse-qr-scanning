<?php

  include '../1Connection.php';
  include '../main_function/login.php';

  $EmpNum = $_SESSION['EmpNum'];
  echo "<span id='emp_num' style='display:none;'>". $EmpNum ."</span>";

  $currentYear = date('Y');
  $currentMonth = date('m');
  $currentDay = date('d');
  $EndDayOfTheMonth = '';

  $MonthName = date("F", mktime(0, 0, 0, $currentMonth, 10));

?>

<div class="import_div">

  <div class="form-group container">
    
      <label id="browse_file" for="fileupload"><i class="fa-solid fa-magnifying-glass"></i> SEARCH FILE</label>
      <input id="fileupload" type=file name="files[]">
      <button id="upload_table"><i class="fa-solid fa-upload"></i> UPLOAD ALL</button>
      <button id="clear_table"><i class="fa-solid fa-trash"></i> CLEAR TABLE</button>
    
  </div>

  <div class="container" style="margin-bottom:15px;">
      <input class="close" id="p_slip_import" type="text" value="IM<?php echo $currentYear . $currentMonth ?>-000" placeholder="PURCHASE">
      <input class="close" id="s_slip_import" type="text" value="INV<?php echo $currentYear . $currentMonth ?>-000" placeholder="SALES">
  </div>

  <div class="w3-responsive">
    <!-- <h2 class="w3-center"><strong>All Items in Stock </strong></h2> -->
    <table id="tblItems" class="table w3-table-all w3-hoverable ui striped table">
      <thead>
        <tr>
          <th>ASSY LINE</th>
          <th>invoice number</th>
          <th data-type="date">date receive</th>
          <th>supplier</th>
          <th>goods code</th>
          <th>materials type</th>
          <th>item code</th>
          <th>part number</th>
          <th>part name</th>
          <th>quantity</th>
          <th>action</th>
        </tr>
      </thead>
      <tbody></tbody>
    </table>
  </div>
</div>

<script>

  var row_id = '';

  var ExcelToJSON = function() {
  this.parseExcel = function(file) {
    var reader = new FileReader();

    reader.onload = function(e) {
      var data = e.target.result;
      var workbook = XLSX.read(data, {
        type: 'binary'
      });
      workbook.SheetNames.forEach(function(sheetName) {
        var XL_row_object = XLSX.utils.sheet_to_row_object_array(workbook.Sheets[sheetName]);
        var productList = JSON.parse(JSON.stringify(XL_row_object));

        var rows = $('#tblItems tbody');
        // console.log(productList)
        for (i = 0; i < productList.length; i++) {
          row_id = i;
          var columns = Object.values(productList[i])

          var date = new Date(Math.round((columns[2] - (25567 + 1)) * 86400 * 1000));
          var converted_date = date.toISOString().split('T')[0];

          // console.log(converted_date);

          rows.append(`
                        <tr id="row${i}">
                            <td id="assy_line${i}">${columns[0]}</td>
                            <td id="invoice${i}">${columns[1]}</td>
                            <td id="date_receive${i}">${converted_date}</td>
                            <td id="supplier${i}">${columns[3]}</td>
                            <td id="goods_code${i}">${columns[4]}</td>
                            <td id="materials${i}">${columns[5]}</td>
                            <td id="item_code${i}">${columns[6]}</td>
                            <td id="part_number${i}">${columns[7]}</td>
                            <td id="part_name${i}">${columns[8]}</td>
                            <td id="quantity${i}">${columns[9]}</td>
                            <td>
                              <span id="span${i}">
                                <button id="${i}" class="use-address positive ui button" onClick="use_click(this.id)">UPLOAD</button>
                              </span>
                              <span id="uploaded${i}" class="close">UPLOADED</span>
                            </td>
                        </tr>
                    `);

                    
        }

      })
    };
    reader.onerror = function(ex) {
      console.log(ex);
    };

    reader.readAsBinaryString(file);
  };
};

function handleFileSelect(evt) {
  var files = evt.target.files; // FileList object
  var xl2json = new ExcelToJSON();
  xl2json.parseExcel(files[0]);
}

document.getElementById('fileupload').addEventListener('change', handleFileSelect, false);

$( "#clear_table" ).click(function() {
  // alert( "Handler for .change() called." );
  $("#tblItems tbody tr").remove(); 
  $( "#fileupload" ).prop( "disabled", false );
  $( "#browse_file" ).removeClass("report_download_press");
});

$( "#fileupload" ).change(function() {
  $( "#fileupload" ).prop( "disabled", true );
  $( "#browse_file" ).addClass("report_download_press");
});

function use_click(clicked_id){

  // console.log(clicked_id);

  var assy_line = $('#assy_line'+clicked_id).text();
  var invoice = $('#invoice'+clicked_id).text();
  var date_receive = $('#date_receive'+clicked_id).text();
  var supplier = $('#supplier'+clicked_id).text();
  var goods_code = $('#goods_code'+clicked_id).text();
  var materials = $('#materials'+clicked_id).text();
  var item_code = $('#item_code'+clicked_id).text();
  var part_number = $('#part_number'+clicked_id).text();
  var part_name = $('#part_name'+clicked_id).text();
  var quantity = $('#quantity'+clicked_id).text();
  
  // console.log(assy_line, invoice, date_receive, supplier, goods_code, materials, item_code, part_number, part_name, quantity);

  $.ajax({
    type: "POST",
    url: '../Ajax_Import/insert_import.php',
    data: {
      assy_line: assy_line,
      invoice: invoice,
      date_receive: date_receive,
      supplier: supplier,
      goods_code: goods_code,
      materials: materials,
      item_code: item_code,
      part_number: part_number,
      part_name: part_name,
      quantity: quantity

    },
    cache: false,
    success: function(data) {
        alert(data);
        $('#row'+clicked_id).addClass("positive");
        $('#span'+clicked_id).addClass("close");
        $('#uploaded'+clicked_id).removeClass("close");
        // this.disabled = true;
        // $("#QR-Code").val('');
        // $("#ajax_table").empty('');
    },
    error: function(xhr, status, error) {
        console.error(xhr);
    }

  });

}

// var p_slip = '';
// var s_slip = '';

// $('#p_slip_import').blur(function () {
//     p_slip = $(this).val(); //or this.value
//     console.log(p_slip);
// });

// $('#s_slip_import').blur(function () {
//     s_slip = $(this).val(); //or this.value
//     console.log(s_slip);
// });

$( "#upload_table" ).click(function() {

  
  var s_slip = $('#s_slip_import').val();
  var p_slip = $('#p_slip_import').val();

  console.log(row_id);

  for (let i = 0; i <= row_id; i++) {
    var assy_line = $('#assy_line'+i).text();
    var invoice = $('#invoice'+i).text();
    var date_receive = $('#date_receive'+i).text();
    var supplier = $('#supplier'+i).text();
    var goods_code = $('#goods_code'+i).text();
    var materials = $('#materials'+i).text();
    var item_code = $('#item_code'+i).text();
    var part_number = $('#part_number'+i).text();
    var part_name = $('#part_name'+i).text();
    var quantity = $('#quantity'+i).text(); 

    console.log(i, assy_line, invoice, date_receive, supplier, goods_code, materials, item_code, part_number, part_name, quantity);

    $.ajax({
      type: "POST",
      url: '../Ajax_Import/insert_import.php',
      data: {
        assy_line: assy_line,
        invoice: invoice,
        date_receive: date_receive,
        supplier: supplier,
        goods_code: goods_code,
        materials: materials,
        item_code: item_code,
        part_number: part_number,
        part_name: part_name,
        quantity: quantity,
        p_slip: p_slip,
        s_slip: s_slip

      },
      cache: false,
      success: function(data) {
          // alert(data);
          $('#row'+i).addClass("positive");
          $('#span'+i).addClass("close");
          $('#uploaded'+i).removeClass("close");
          // this.disabled = true;
          // $("#QR-Code").val('');
          // $("#ajax_table").empty('');
      },
      error: function(xhr, status, error) {
          console.error(xhr);
      }

    });

  } 

});

</script>

<!-- CHECKER KUNG SI 7113 ANG NAG LOG IN PARA SA PURCHASE SA SALES SLIP FIELDS -->
<script>
    var User = document.getElementById('emp_num');
    var User_value = User.textContent;

    console.log(User_value);

    if( User_value == '7113' ){

        $("#s_slip_import").removeClass("close");
        $("#p_slip_import").removeClass("close");
        
    }
</script>