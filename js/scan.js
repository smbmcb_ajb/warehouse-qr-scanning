let scanner = new Instascan.Scanner({ video: document.getElementById('preview'), mirror: false });
scanner.addListener('scan', function (content) {
    //alert(content);
    document.getElementById('QR-Code').value = content;

    var Code = content;

    var shutter = new Audio();
    shutter.autoplay = true;
    shutter.src = navigator.userAgent.match(/Firefox/) ? 'audio/read.mp3' : 'audio/read.ogg';

    $.ajax({
        type: "POST",
        url: '../Ajax_Scanner/ajax_table.php',
        data: {
                Code: Code
        },
        cache: false,
        success: function(data) {

            $(".ajax_table_mobile").html(data);
            $(".ajax_table_desktop").html(data);
            // var shutter = new Audio();
            // shutter.autoplay = true;
            // shutter.src = navigator.userAgent.match(/Firefox/) ? 'audio/read.mp3' : 'audio/read.ogg';
            // $("#preview").addClass("detected");

        },
        error: function(xhr, status, error) {
            console.error(xhr);
        }

    });

});

$(".Scan-Button").click(function(){
    // $(".frame-container").removeClass("close");
    Instascan.Camera.getCameras().then(function (cameras) {
        if (cameras.length > 0) {
            if (cameras[1]) {
                //use that by default
                scanner.start(cameras[1]);
                $(".Scan-Button").addClass("Press_Status");
                $(".Stop-Button").removeClass("Press_Status");
                $(".frame-container").removeClass("close");
                $("#ajax_search_result").addClass("close");
                // $("#preview").removeClass("detected");
            } else {
                //else use front camera
                scanner.start(cameras[0]);
                $(".Scan-Button").addClass("Press_Status");
                $(".Stop-Button").removeClass("Press_Status");
                $(".frame-container").removeClass("close");
                $("#ajax_search_result").addClass("close");
                // $("#preview").removeClass("detected");
            }
        } 
        else {
            console.error('No cameras found.');
        }
    }).catch(function (e) {
        console.error(e);
    });
});

$(".Stop-Button").click(function(){
    scanner.stop()
    $(".Stop-Button").addClass("Press_Status");
    $(".Scan-Button").removeClass("Press_Status");

    $(".frame-container").addClass("close");
});


// $("#QR-Code").on("input", function() {

//     var Code = $(this).val();

//     $.ajax({
//         type: "POST",
//         url: 'Ajax_Scanner/ajax_table.php',
//         data: {
//                 Code: Code
//         },
//         cache: false,
//         success: function(data) {

//             $(".ajax_table_mobile").html(data);
//             $(".ajax_table_desktop").html(data);

//         },
//         error: function(xhr, status, error) {
//             console.error(xhr);
//         }

//     });
//  });

 $("#search_item").click(function() {

    var Code = $('#QR-Code').val();

    $.ajax({
        type: "POST",
        url: '../Ajax_Scanner/ajax_table.php',
        data: {
                Code: Code
        },
        cache: false,
        success: function(data) {
            // $(".ajax_table_mobile").html(data);
            // $(".ajax_table_desktop").html(data);
            $(".ajax_search_result").html(data);

        },
        error: function(xhr, status, error) {
            console.error(xhr);
        }

    });

    $.ajax({
        type: "POST",
        url: '../Ajax_Scanner/search_result.php',
        data: {
                Code: Code
        },
        cache: false,
        success: function(data) {

            $("#ajax_search_result").html(data);

        },
        error: function(xhr, status, error) {
            console.error(xhr);
        }

    });

 });


// HTML5QRSCANNING SCRIPT

// function docReady(fn) {
//     // see if DOM is already available
//     if (document.readyState === "complete"
//     || document.readyState === "interactive") {
//         // call on next available tick
//         setTimeout(fn, 1);
//     } 
//     else {
//         document.addEventListener("DOMContentLoaded", fn);
//         //document.getElementById(‘text’).value=c;
//     }
// }

// docReady(function () {
//     var resultContainer = document.getElementById('result');
//     var lastResult, countResults = 0;
//     function onScanSuccess(decodedText, decodedResult) {
//         if (decodedText !== lastResult) {
//             ++countResults;
//             lastResult = decodedText;
//             // Handle on success condition with the decoded message.
//             console.log('Scan result ${decodedText}', decodedResult);
//             // document.getElementById("result").textContent=lastResult;
//             document.getElementById('QR-Code').value = lastResult;

//             var Code = lastResult;

//             var shutter = new Audio();
//             shutter.autoplay = true;
//             shutter.src = navigator.userAgent.match(/Firefox/) ? 'audio/read.mp3' : 'audio/read.ogg';

//             $.ajax({
//                 type: "POST",
//                 url: 'Ajax_Scanner/ajax_table.php',
//                 data: {
//                     Code: Code
//                 },
//                 cache: false,
//                 success: function(data) {
//                     // alert(data);
//                     $("#ajax_table").html(data);
//                     // $("#preview").addClass("detected");
                
//                 },
//                 error: function(xhr, status, error) {
//                     console.error(xhr);
//                 }
                
//             });

//         }

//     }

// var html5QrcodeScanner = new Html5QrcodeScanner(
// "reader", { fps: 10, qrbox: 250 });
// html5QrcodeScanner.render(onScanSuccess);

// });