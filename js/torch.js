const SUPPORTS_MEDIA_DEVICES = 'mediaDevices' in navigator;

if (SUPPORTS_MEDIA_DEVICES) {
//Get the environment camera (usually the second one)
    navigator.mediaDevices.enumerateDevices().then(devices => {
    
        const cameras = devices.filter((device) => device.kind === 'videoinput');

        if (cameras.length === 0) {
            throw 'No camera found on this device.';
        }

        const camera = cameras[cameras.length - 1];

        // Create stream and get video track
        navigator.mediaDevices.getUserMedia({
            video: {
                deviceId: camera.deviceId,
                facingMode: ['user', 'environment'],
                height: {ideal: 1080},
                width: {ideal: 1920}
            }
        }).then(stream => {
            const track = stream.getVideoTracks()[0];

            //Create image capture object and get camera capabilities
            const imageCapture = new ImageCapture(track)
            const photoCapabilities = imageCapture.getPhotoCapabilities().then(() => {

                

                //let there be light!
                var light_On_Off = 0;
                const btn = document.querySelector('.Div-Flash');
                btn.addEventListener('click', function(){
                    if (light_On_Off == 0){
                        track.applyConstraints({
                            advanced: [{torch: true}]
                        });
                        light_On_Off = 1;
                        $(".FLash-Button").addClass("Press_Status");
                    }
                    else {
                        track.applyConstraints({
                            advanced: [{torch: false}]
                        });
                        light_On_Off = 0;
                        $(".FLash-Button").removeClass("Press_Status");
                    }
                    
                });
            
            });
        });
    });
}