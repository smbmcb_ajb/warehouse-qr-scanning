<?php

include '1Connection.php';
include 'main_function/login.php';

if(!isset($_SERVER['HTTP_REFERER'])){
    // redirect them to your desired location
    header('location: index.php');
    exit;
}

if(!isset($_SESSION['EmpNum'])){
    header("Location:index.php");
}

if($_SERVER['REQUEST_METHOD'] == "POST" and isset($_POST['logout_desktop'])) {
    func();
}

function func()
{
    echo "<script>alert('logout')</script>";
    session_start();
    session_destroy();
    unset($_SESSION["EmpNum"]);
    unset($_SESSION["FullName"]);
    unset($_SESSION["Admin"]);
    header("Location: index.php");    
}

$EmpNum = $_SESSION['EmpNum'];
$Name = $_SESSION['FullName'];
$Admin = $_SESSION['Admin'];

if(isset($_POST["Logout"])){
    unset($_SESSION["EmpNum"]);
    unset($_SESSION["FullName"]);
    header("Location: index.php");
}

if(isset($_POST["SIGNOUT"])){
    unset($_SESSION["EmpNum"]);
    unset($_SESSION["FullName"]);
    header("Location: index.php");
}

$currentYear = date('Y');
$currentMonth = date('m');
$currentDay = date('d');
$EndDayOfTheMonth = '';

$MonthName = date("F", mktime(0, 0, 0, $currentMonth, 10));

?>

<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <!-- <meta name="color-scheme" content="dark"> -->
    <title>Warehouse Main Page</title>

    <!-- XLSX -->
    <script src="https://cdn.jsdelivr.net/npm/xlsx@0.18.0/dist/xlsx.full.min.js"></script>
    

    <!-- EXTERNAL LINK FOR FONT AWESOME AND SEMANTIC UI-->  
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/semantic-ui/1.11.8/semantic.min.css"/> 

    <!-- FONT AWESOME 5 -->
    <script src="https://kit.fontawesome.com/9b0df8280e.js" crossorigin="anonymous"></script>

    <!-- JQUERY / INSTASCAN / SEMANTIC UI / VUE JS / ANGULAR -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
    <script src="https://rawgit.com/schmich/instascan-builds/master/instascan.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/semantic-ui/1.11.8/semantic.min.js"></script>
    <script src="https://cdn.jsdelivr.net/npm/vue@2.6.14"></script>
    <script src="https://ajax.googleapis.com/ajax/libs/angularjs/1.7.8/angular.min.js"></script>


    <!-- JQUERY DATATABLE PLUGIN -->
    <script type="text/javascript" src="https://cdn.datatables.net/1.10.22/js/jquery.dataTables.min.js"></script>
    <link rel="stylesheet" href="https://cdn.datatables.net/1.10.22/css/jquery.dataTables.min.css" />


    <script src="//cdn.rawgit.com/rainabba/jquery-table2excel/1.1.0/dist/jquery.table2excel.min.js"></script>


    <!-- HTML 5 QR SCANNER -->
    <!-- <script src="https://raw.githubusercontent.com/mebjas/html5-qrcode/master/minified/html5-qrcode.min.js"></script>
    <script src="https://unpkg.com/html5-qrcode" type="text/javascript"></script> -->

    <!-- EXTERNAL JS -->
    <script type="text/javascript" src="js/scan.js" async></script>
    <!-- <script type="text/javascript" src="js/torch.js" async></script> -->
    <script type="text/javascript" src="js/user_modal.js" async></script>
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.6.2/dist/css/bootstrap.min.css" integrity="sha384-xOolHFLEh07PJGoPkLv1IbcEPTNtaed2xpHsD9ESMhqIYd0nLMwNLD69Npy4HI+N" crossorigin="anonymous">
    <!-- EXTERNAL CSS -->
    <link rel="stylesheet" href="css/main_page.css" />
    <link rel="stylesheet" href="css/scan.css" />
    <link rel="stylesheet" href="css/global.css" />
    <link rel="stylesheet" href="css/ajax_table.css" />
    <link rel="stylesheet" href="css/user_modal.css" />
    <link rel="stylesheet" href="css/menu.css" />
    <link rel="stylesheet" href="css/report.css" />
    <link rel="stylesheet" href="css/slideshow.css" />
    <link rel="stylesheet" href="css/edit.css" />
    <link rel="stylesheet" href="css/import.css" />
    <link rel="stylesheet" href="css/report_table.css" />
    <link rel="stylesheet" href="css/ajax_user_desktop.css" />
    <link rel="stylesheet" href="css/search_result.css" />
    <link rel="stylesheet" href="css/datatables.css" />

    <!-- FAVICON -->
    <link rel="shortcut icon" type="image/jpg" href="favicon_io/android-chrome-512x512.png"/>
    
</head>

<body>



    <!-- SCANNING PART USING HTML5 QR SCANNER -->
    <!-- <div id="preview"></div>
    <div id="result"></div> -->
    
    <!-- NAVIGATION MENU -->
    <header>
        <nav>
            <div id="navbar" class="vertical_center">
                <div id="logo" class="reverse">
                    <div class="system_title">
                        Warehouse Receiving
                    </div>
                </div>
                
                <div class="user_div close">
                    <a href="#demo-modal">
                        <i class="fa-solid fa-circle-user"></i> <br> 
                    </a>
                </div>
                
            </div>

            <div id="links">
                <button class="nav_menu close" id="slip_edit">Slip Edit</button>
                <button class="nav_menu close" id="iqc_data">IQC Data</button>
                <button class="nav_menu" id="receive_desktop">Receiving</button>
                <button class="nav_menu" id="report_desktop">Reports</button>
                <button class="nav_menu" id="print_desktop">Print QR</button>
                <button class="nav_menu close" id="to_be_receive_desktop">Incoming Materials</button>
                <button class="nav_menu close" disabled id="import_desktop">Import</button>
                <button class="nav_menu close" id="download_desktop">Download</button>
                <form action="" method="post">
                    <button name="logout_desktop" class="nav_menu btn btn-danger" id="logout_desktop">Logout</button>
                </form>
            </div>
            <button name="logout_desktop" class="nav_menu close" id="logout_desktop">Logout</button>
            
        </nav>

    </header>

    <!-- NEW USER MODAL -->
    <div class="modal">
        <div class="modal-content">
            <span class="close-button">×</span>

            <div class="img_container">
                <img src="https://wwbmmc.ca/wp-content/uploads/2020/12/kisspng-computer-icons-avatar-icon-design-male-teacher-5ade176c636ed2.2763610715245044284073.png" 
                alt="<?php echo $Name; ?>"> 
            </div>

            <div class="user_info">

                <div>
                    <span class="user_label">
                        Name: <span class="user_content"><?php echo $Name; ?></span>
                    </span>
                </div>
                <hr>
                <div>
                    <span class="user_label">
                        Employee Number:
                        <span class="user_content"><?php echo $EmpNum; ?></span>
                    </span>
                </div>

                
                
            </div>

            <form action="index.php" method="post">
                <button class="Logout" name="Logout">LOG OUT</button>
            </form>

        </div>
    </div>

    <!-- MENU FOR REPORT / RECEIVING / EXPORT -->
    <div class="menu_div container">
        <div class="Col-1-50 report_div">
            <button id="reports"><span id="report_text">Reports</span></button>
        </div>

        <div class="Col-2-50 download_div">
            <button id="receive" >Receiving</button>
        </div>
        
    </div>

    <!-- SCAN USING INSTASCAN -->
    <div class="QR-SCANNER-DIV close">

        <div class="container_in">

            <div class="Col-1-33_in">
                <div class="frame-container">
                    <p class="Label close">CAMERA AREA</p>
                    <video id="preview"></video>
                </div>
            </div>

            <div class="Col-2-33_in">

            <div class="container Button-Scan-Container">

                <div class="Col-1-50 Div-Scan" style="margin-right:10px;">

                    <button class="Scan-Button"> 
                        <i class="fa-solid fa-qrcode close"></i>
                        <span class="button_text">SCAN QR</span>
                    </button>

                </div>

                <div class="Col-2-50 Div-Stop" style="margin-left:10px;">

                    <button class="Stop-Button"> 
                        <i class="fa-solid fa-square-xmark close"></i>
                        <span class="button_text">CLOSE</span>
                    </button>

                </div>

                <div class="Col-3-33 Div-Flash close">

                    <button class="FLash-Button"> 
                        <i class="fa-solid fa-lightbulb"></i> <br> 
                        <span class="button_text">FLASH</span>
                    </button>

                </div>

                </div>

                <div class="form_part"> 
                    <div class="container">
                        <div class="Col-1-70">
                            <input type="text" value="" id="QR-Code" name="QR-Code" placeholder="Search P.O / QR">
                        </div>
                        <div class="Col-2-30">
                            <button id="search_item"><i class="fa-solid fa-magnifying-glass close"></i> SEARCH</button>
                            <!-- <button class="close" id="count_words"><i class="fa-solid fa-magnifying-glass close"></i> COUNT</button> -->
                        </div>
                    </div>
                </div>

                <div id="ajax_search_result"></div>

            </div>

            <div class="Col-3-33_in">
                <div class="center"><p class="info_area_text">Information Area</p></div>
                <div class="ajax_table_desktop" id="ajax_table"></div>
            </div>

        </div>

        <div class="ajax_table_mobile" id="ajax_table"></div>

    </div>

    <!-- AJAX FOR DATA VIEWING OR REPORT -->
    <div class="ajax_report"></div>


    <!-- DIV PARA SA SLIDER NA MAY INSTRUCTION ON HOW TO USE MOBILE -->
    <div class="container" id="slide_show">
        <div class="frame">
            <div class="scroller">

                <div class="element" id="slide1">
                    <div class="slide_content">
                        <h1 class="title">WAREHOUSE RECEIVING SYSTEM</h1>
                        <p class="color_000">If you haven't been informed on how to use this app,<br> click the <b> "Learn More" </b> option. </p> 
                        <p class="color_000"> If you have any questions or concerns about this system, <br/> you can call the developer (<b>536</b>).</p> 
                        <span><button id="learn_more_desktop">Learn More <i class="fa-solid fa-arrow-right"></i></button></span>

                        <p style="margin-top:20px; font-weight:700; color:#880808;" class="close"> 
                            <i class="fa-solid fa-triangle-exclamation"></i>
                            Sorry for the inconvenience, but the "Import" option is now unavailable, with no indication on when it will be restored.
                        </p>
                    </div>
                </div>

                <div class="element" id="slide2">
                    
                        <h2 class="title_steps">MATERIAL RECEIVING</h2>

                        <img class="image_instruction" src="images/receiving_image/receiving.png" alt="">

                        <p class="color_fff">
                            <b>Step 1: </b>
                            To access the Receiving page, click the 
                            <b>"Receiving"</b>
                            button, which has a 
                            <b>"Warehouse"</b> 
                            icon.
                        </p> 

                        <hr>

                        <img class="image_instruction" src="images/receiving_image/receive_default.png" alt="">

                        <p class="color_fff">
                            The default page of <b>"Receiving"</b> comprises one input box for manually entering item codes or scanning them with a scanner, 
                            and three buttons: one for <b>"QR CODE"</b> icon, which opens the camera, 
                            one for <b>"X"</b> icon, which closes the camera, 
                            and one for <b>"BULB"</b> symbol, which opens the phone's flash.
                        </p> 

                        <hr>

                        <a href="#slide3"><button id="next">Next</button></a>
                    
                </div>

                <div class="element" id="slide3">
                    <p>Box 3</p>
                    <a href="#slide1"><button id="learn_more">Complete <i class="fa-solid fa-check"></i></button></a>
                </div>

            </div>
        </div>
    </div>

    <div id="ajax_user_manual_desktop"></div>

    <!-- // QR Code generator -->
    <div id="ajax_printQR"></div>

<!-- Modal -->
<div class="modal fade" id="staticBackdrop" data-backdrop="static" data-keyboard="false" tabindex="-1" aria-labelledby="staticBackdropLabel" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="staticBackdropLabel" style="color: #3C5393; font-weight:bold;">Traceability QR Code Generator</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div id="printableDiv" class="modal-body w-75 text-center">
    
        <div class="row" style="box-shadow: 3px 3px 10px #888888; height:140px;">
            <div id='color-container' class="col-4" style="border:1px solid black;height:140px;">
                <div class="row" style="height:35px;">
                    <div id="initialBox" class="col-6 justify-content-center d-flex align-items-center" style="border:1px solid black; height:35px; font-weight:bold;">
                        1
                    </div>
                    <div id="totalBox" class="col-6 justify-content-center d-flex align-items-center" style="border:1px solid black; height:35px; font-weight:bold;">
                        
                    </div>  
                </div>
                <div class="row" style="height:35px;">
                    <div id="invoice-container" class="col-12 justify-content-center d-flex align-items-center" style="border:1px solid black; height:35px;">
                        
                    </div> 
                </div>
                <div class="row" style="height:35px;">
                    <div id="dateReceived" class="col-12 justify-content-center d-flex align-items-center" style="border:1px solid black; height:35px;">
                        Date Receive
                    </div> 
                </div>
                <div class="row" style="height:35px;">
                    <div id="commodityCode" class="col-12 justify-content-center d-flex align-items-center" style="border:1px solid black; height:35px; font-weight:bold;">
                        
                    </div> 
                </div>
            </div>
            <div class="col-4 justify-content-center d-flex align-items-center" style="height:140px">
                
            </div>
            <div class="col-4 justify-content-center d-flex align-items-center" style="height:140px;">
                <div id="qrcode">
                    
                </div>
                <div style="display:none;" id="qrcode-for-print">
                    
                </div>
            </div>
            <!-- <div class="col">
                <div id="qr-code" class="mx-auto text-center">
                    
                </div>
            </div> -->
        </div>
        <!-- <div class="row mx-auto">
            <div class="col" style="margin-left:13%;"><svg id="barcode"></svg></div>
        </div> -->
        
      </div>
      <div class="row">
            <div class="col-12 text-center">
                <form>
                    <div class="form-group mx-auto">

                        <!-- <label for="numberOfBox" class="col-sm-12 col-form-label">Number of box: </label>
                        <div class="col-sm-4 mx-auto">
                            <input type="text"  class="form-control text-center" id="numberOfBox" style="box-shadow: none; border-color:#414141;">
                        </div> -->
                        <div class="row d-flex justify-content-center">
                            <!-- <div class="col-5" style="border-right:1px solid black;">
                                <label for="boxNumber" class="col-sm-12 col-form-label">Box Number: </label>
                                <div class="col-sm-5 mx-auto" >
                                    <input type="text" oninput="boxDetailChanger()"  class="form-control text-center" id="boxNumber" style="box-shadow: none; border-color:#414141;">
                                </div>
                            </div> -->
                            <div class="col-12">
                                <label for="totalNumberOfBox" class="col-sm-12 col-form-label">Total Number of Box: </label>
                                <div class="col-sm-5 mx-auto">
                                    <input type="text" oninput="boxDetailChanger()"  class="form-control text-center" id="totalNumberOfBox" style="box-shadow: none; border-color:#414141;" autofocus>
                                </div>
                            </div>
                            <div class="col-12 mt-3">
                                <label id="error-container" style="height: 50px; background-color:#ffebe8;border: 1px solid rgb(219, 84, 84);border-radius: 7px;font-weight: bold;display:none;" class="col-sm-12 col-form-label justify-content-center align-items-center"></label>
                            </div>
                        </div>
                        <!-- <label for="numberOfItems" class="col-sm-12 col-form-label">Number of pieces to be printed:</label> -->
                        <!-- <div class="col-sm-4 mx-auto">
                            <input type="text" class="form-control text-center" id="numberOfItems" style="box-shadow: none; border-color:#414141;">
                        </div> -->
                        <!-- <input type="hidden" class="form-control text-center" id="quantityContainer" style="box-shadow: none; border-color:#414141;"> -->
                    </div>
                    <!-- <div class="form-group mx-auto">
                        
                    </div> -->
                </form>
            </div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancel</button>
        <button onclick="printQR()" type="button" class="btn" id="btnPrint" style="background-color: #363795; color:white;" disabled>Print</button>
      </div>
    </div>
  </div>
</div>
<!-- // QR Code generator -->

<!-- SCRIPT PARA SA MOBILE MENU -->
<script>
    function openNav() {
        document.getElementById("mySidenav").style.width = "250px";
    }

    function closeNav() {
        document.getElementById("mySidenav").style.width = "0";
    }
</script>

<!-- REPORT / RECEIVING / DOWNLOAD / IMPORT BUTTON -->
<script>

    var urlReport = "Ajax_Report/report.php"
    var urlReport_Table = "Ajax_Report/report_table.php"
    var urlImport_Table = "Ajax_Import/import.php"
    var urlIQC_Data = "/Ajax_IQC/ajax_iqc_data.php"
    var urlEdit_Slip = "/Ajax_Slip_Edit/ajax_slip_edit.php"

    // $( document ).ready(function() {
	//     $(".ajax_report").load(urlReport_Table);
    //     $("#report_desktop").addClass("report_download_press");
    // });

    // MOBILE MENU

    $("#receive").click(function(){

        if($("#receive").hasClass("report_download_press")){

            $(".QR-SCANNER-DIV").addClass("close");
            $("#reports").removeClass("close");
            $("#receive").removeClass("report_download_press");

            $(".frame").removeClass("close");

        }

        else{

            $(".QR-SCANNER-DIV").removeClass("close");
            $("#reports").addClass("close");
            $("#receive").addClass("report_download_press");

            $(".frame").addClass("close");

        }

    });

    $("#reports").click(function(){

        if ($("#reports").hasClass("report_download_press")){

            $("#reports").removeClass("report_download_press");
            $("#import").addClass("close");
            $("#import").removeClass("report_download_press");

            // $(".QR-SCANNER-DIV").removeClass("close");

            $(".ajax_report").empty(urlReport);

            $("#report_text").text("Reports");
            $("#report_icon").addClass("fa-chart-simple");
            $("#report_icon").removeClass("fa-chevron-left");

            $("#receive").removeClass("close");

            $(".frame").removeClass("close");

        }

        else{

            $("#reports").addClass("report_download_press");
            $("#import").removeClass("close");

            $(".QR-SCANNER-DIV").addClass("close");

            $(".ajax_report").load(urlReport);

            $(".ajax_report").css({"display": "block"});

            // $("#report_text").text("Back");
            $("#report_icon").removeClass("fa-file-excel");
            $("#report_icon").addClass("fa-chevron-left");

            $("#receive").addClass("close");

            $(".frame").addClass("close");

        }
        
    });

    $("#import").click(function(){

        // $("#import").addClass("report_download_press");

        if($("#import").hasClass("report_download_press")){

            $(".QR-SCANNER-DIV").addClass("close");
            $(".ajax_report").empty(urlReport);

            $("#import").removeClass("report_download_press");
            $("#import").addClass("close");

            $(".ajax_report").css({"display": "flex"});

            $(".frame").removeClass("close");

            $("#receive").removeClass("report_download_press");
            $("#reports").removeClass("report_download_press");
            $("#receive").removeClass("close");
            $("#reports").removeClass("close");

            $("#report_icon").addClass("fa-chart-simple");
            $("#report_icon").removeClass("fa-chevron-left");

        }

        else{

            $(".frame").addClass("close");

            $(".ajax_report").empty(urlReport);
            $(".QR-SCANNER-DIV").addClass("close");

            $(".ajax_report").load(urlImport_Table);
            $(".ajax_report").css({"display": "block"});

            $("#import").addClass("report_download_press");

            $("#receive").removeClass("report_download_press");
            $("#reports").removeClass("report_download_press");
            $("#reports").addClass("close");

        }
    });

    // DESKTOP MENU

    $("#receive_desktop").click(function(){

        if($("#receive_desktop").hasClass("report_download_press")){

            $(".QR-SCANNER-DIV").addClass("close");

            $(".ajax_report").empty(urlReport);

            $("#report_desktop").removeClass("report_download_press");

            $("#receive_desktop").removeClass("report_download_press");
            
            $(".frame").removeClass("close");

            $("#ajax_user_manual_desktop").empty();
            $("#ajax_user_manual_desktop").removeClass("close");

        }

        else{

            $("#ajax_user_manual_desktop").addClass("close");

            $(".QR-SCANNER-DIV").removeClass("close");


            $(".ajax_report").empty(urlReport);

            $("#report_desktop").removeClass("report_download_press");
            $("#import_desktop").removeClass("report_download_press");
            $("#iqc_data").removeClass("report_download_press");
            $("#slip_edit").removeClass("report_download_press");

            $(".import_div").addClass("close");
            
            $("#receive_desktop").addClass("report_download_press");

            $(".frame").addClass("close");

        }

    });

    $("#report_desktop").click(function(){

        if($("#report_desktop").hasClass("report_download_press")){

            $(".QR-SCANNER-DIV").addClass("close");
            $(".ajax_report").empty(urlReport);

            $("#report_desktop").removeClass("report_download_press");
            
            $(".frame").removeClass("close");

            $("#ajax_user_manual_desktop").empty();
            $("#ajax_user_manual_desktop").removeClass("close");

        }

        else{

            $("#ajax_user_manual_desktop").addClass("close");

            $(".QR-SCANNER-DIV").addClass("close");

            $(".ajax_report").load(urlReport_Table);

            $(".ajax_report").empty(urlImport_Table);
            
            $("#report_desktop").addClass("report_download_press");
            $("#receive_desktop").removeClass("report_download_press");
            $("#import_desktop").removeClass("report_download_press");
            $("#iqc_data").removeClass("report_download_press");
            $("#slip_edit").removeClass("report_download_press");

            $(".import_div").addClass("close");
            $(".frame").addClass("close");

            $(".ajax_report").css({"display": "flex"});

        }

    });

    $("#import_desktop").click(function(){

        if($("#import_desktop").hasClass("report_download_press")){

            $(".QR-SCANNER-DIV").addClass("close");
            $(".ajax_report").empty(urlReport);

            $("#import_desktop").removeClass("report_download_press");

            $(".ajax_report").css({"display": "flex"});
            
            $(".frame").removeClass("close");

            $("#ajax_user_manual_desktop").empty();
            $("#ajax_user_manual_desktop").removeClass("close");

        }

        else{

            $("#ajax_user_manual_desktop").addClass("close");
            
            $(".frame").addClass("close");

            $(".ajax_report").empty(urlReport);
            $(".QR-SCANNER-DIV").addClass("close");
            
            $(".ajax_report").load(urlImport_Table);
            $(".ajax_report").css({"display": "block"});
            
            $("#import_desktop").addClass("report_download_press");

            $("#receive_desktop").removeClass("report_download_press");
            $("#report_desktop").removeClass("report_download_press");
            $("#iqc_data").removeClass("report_download_press");
            $("#slip_edit").removeClass("report_download_press");

        }

    });

    $("#iqc_data").click(function(){

        if($("#iqc_data").hasClass("report_download_press")){

            $(".ajax_report").empty(urlIQC_Data);
            
            $(".frame").removeClass("close");

            $("#iqc_data").removeClass("report_download_press");

            $("#iqc_data").removeClass("report_download_press");

        }

        else{

            $("#ajax_user_manual_desktop").addClass("close");

            $(".ajax_report").load(urlIQC_Data);
            
            $(".frame").addClass("close");

            $(".QR-SCANNER-DIV").addClass("close");
            
            $("#iqc_data").addClass("report_download_press");

            $("#receive_desktop").removeClass("report_download_press");
            $("#report_desktop").removeClass("report_download_press");
            $("#import_desktop").removeClass("report_download_press");
            $("#slip_edit").removeClass("report_download_press");

        }

    });

    $("#slip_edit").click(function(){

        if($("#slip_edit").hasClass("report_download_press")){

            $(".ajax_report").empty(urlEdit_Slip);
            
            $(".frame").removeClass("close");

            $("#slip_edit").removeClass("report_download_press");

            $("#slip_edit").removeClass("report_download_press");
            $("#receive_desktop").removeClass("report_download_press");
            $("#report_desktop").removeClass("report_download_press");
            $("#import_desktop").removeClass("report_download_press");
            $("#iqc_data").removeClass("report_download_press");

        }

        else{

            $("#ajax_user_manual_desktop").addClass("close");

            $(".ajax_report").load(urlEdit_Slip);
            
            $(".frame").addClass("close");

            $(".QR-SCANNER-DIV").addClass("close");
            
            $("#slip_edit").addClass("report_download_press");

            $("#receive_desktop").removeClass("report_download_press");
            $("#report_desktop").removeClass("report_download_press");
            $("#import_desktop").removeClass("report_download_press");
            $("#iqc_data").removeClass("report_download_press");

        }

    });

// QR Generator
document.getElementById('print_desktop').addEventListener('click',()=>{
    // console.log("ddddd");
    // $(".menu_div").addClass("close");
    // $(".ajax_report").addClass("close");
    // $(".container_in").addClass("close");
    // $("#ajax_user_manual_desktop").addClass("close");
    window.location = "../qrCode_generator/print_qr.php"
})

</script>

<!-- SCRIPT PARA SA PAG CREATE NG EXCEL FILE -->
<script>    
   

    const monthNames = ["January", "February", "March", "April", "May", "June",
    "July", "August", "September", "October", "November", "December"
    ];

    const d = new Date();
    // document.write("The current month is " + monthNames[d.getMonth()]);

    var Month =  monthNames[d.getMonth()];

    $("#download_desktop").click(function() {

        console.log(Month);

        $("#table_export").table2excel({
            filename: "Table.xls"
        });

    });
   
</script>


<!-- SCRIPT PARA SA IMPORT TABLE FROM EXCEL TO TABLE -->
<script>
    $( document ).ready(function() {
	    $(".export").click(function() {
		    var export_type = $(this).data('export-type');		
		    $('#data_table').tableExport({
                type : export_type,			
                escape : 'false',
                ignoreColumn: []
		    });		
        });
    });
</script>

<!-- SCRIPT FOR USER MANUAL -->
<script>

    var urlUserManualDesktop = "Ajax_User_Manual/ajax_user_desktop.php"

    $("#learn_more_desktop").click(function(){
        $(".frame").addClass("close");

        $("#ajax_user_manual_desktop").load(urlUserManualDesktop);
    });
</script>

<!-- SCRIPT PARA SA MULTIPLE VALUE NG ISANG QR -->
<script>

    $(document).ready(function(){
        $("#count_words").click(function(){
            var words = $.trim($("#QR-Code").val()).split(" ");
            // alert(words.length);
            // var counter = words.length;
            // console.log(counter);
            for (let i = 0; i < words.length; i++) { 
                console.log(words[i]);
            }
        });
    });

</script>

<?php
    echo "<span id='EmpNum' style='display:none;'> $EmpNum </span>";
?>

<script>
    var EmpNum = document.getElementById('EmpNum');
    var EmpNum_value = EmpNum.textContent;

    

    if( EmpNum_value == ' 7113 '){

        $("#iqc_data").removeClass("close");
        $("#slip_edit").removeClass("close");
        
    }
    else if( EmpNum_value == ' 1748 ' ||
             EmpNum_value == ' 8585 ' ||
             EmpNum_value == ' 2668 ' ||
             EmpNum_value == ' 291 ' ||
             EmpNum_value == ' 7527 ' ||
             EmpNum_value == ' 4090 ' ){

        $("#iqc_data").removeClass("close");

    }
    else {

        $("#iqc_data").addClass("close");
        $("#slip_edit").addClass("close");
        
    }

    

</script>

<!-- QR code generator -->
<script>
    
    // let totalQuantity = parseInt(document.getElementById("quantityContainer").value)
    // console.log(typeof(totalQuantity))
    // let totalNumberOfBox = parseInt(document.getElementById("numberOfBox").value)
    // let numberOfItemsPerBox = parseInt(document.getElementById("numberOfItems").value)
    let printQR = ()=>{
        // var QrContents = document.getElementById("printableDiv").innerHTML;
        var a = window.open('', 'sample', 'height=auto, width=auto, margin=0');
        a.document.write('<html>');
        a.document.write('<head>');
        // a.document.write('<link rel="stylesheet" href="css/print_design.css" />')
        a.document.write('</head>');
        a.document.write("<body>");
        // a.document.write("<div>");
        // column-count:2; flex-direction:row; width:816px; height:1056px; flex-wrap:wrap;align-content: flex-start;'

        for(i=0; i<document.getElementById('totalNumberOfBox').value; i++){
            a.document.write("<div style='display:inline-block;'>");
            a.document.write("<div style='max-width:380px; display:flex; flex-direction:row; height: 111px;'>");
                a.document.write("<div id='first-row-to-print' style='width:130px;border:1px solid black; background-color:"+ document.getElementById('color-container').style.backgroundColor +";color:"+ document.getElementById('color-container').style.color +"'>");
                    a.document.write("<div style='height: 28px;display:flex; flex-direction:row;'>")
                        a.document.write("<div style='height: 28px;width:65px; border-right:1px solid black;display:flex;justify-content:center; align-items:center;'>")
                            a.document.write(i + 1);
                        a.document.write("</div>");
                        a.document.write("<div style='height: 28px;width:65px;border-left:1px solid black;display:flex;justify-content:center; align-items:center;'>")
                            a.document.write(document.getElementById('totalBox').textContent);
                        a.document.write("</div>")
                    a.document.write("</div>")
                    a.document.write("<div style='height: 27px;border-top:1px solid black;border-bottom:0;display:flex;justify-content:center; align-items:center;'>")
                        a.document.write(document.getElementById('invoice-container').textContent);
                    a.document.write("</div>")
                    a.document.write("<div style='height: 27px;border-top:1px solid black;border-bottom:0;display:flex;justify-content:center; align-items:center;'>")
                        a.document.write(document.getElementById('dateReceived').textContent);
                    a.document.write("</div>")
                    a.document.write("<div style='height: 28px;border-top:1px solid black;display:flex;justify-content:center; align-items:center;'>")
                        a.document.write(document.getElementById('commodityCode').textContent);
                    a.document.write("</div>")
                a.document.write("</div>")
                a.document.write("<div style='width:130px;border: 1px solid black;'>");

                a.document.write("</div>")
                a.document.write("<div style='width:130px;border: 1px solid black;display:flex;justify-content:center; align-items:center;'>");
                    a.document.write(document.getElementById('qrcode-for-print').innerHTML);
                a.document.write("</div>")
            a.document.write("</div>")
            a.document.write("</div>&nbsp");
            
                // if(i % 3 == 0){
                // a.document.write("<p style='page-break-before: always;'></p>")
                // }
        }
        
        a.document.write('</body></html>');
        // setTimeout(()=>{
            
        // }, 2000)
        
        a.document.close();
        setTimeout(()=>{
            a.print();
        }, 3000)
        
        

        // for(i=0; i<document.getElementById('totalNumberOfBox').value; i++){
        //     a.document.write('<table id="printTable" style="border:1px solid black; width:200px; height: 111px;">')
        //         a.document.write("<tr style=' border:1px solid red;'>")
        //             a.document.write(`<td style="border:1px solid black;width:66px;">${i+1}</td>`)
        //             a.document.write(`<td style='width:66px'>${i+1}</td>`);
        //             a.document.write(`<td style='width:66px'>${i+1}</td>`)
        //         a.document.write("</tr>")
        //     a.document.write("</table>")
        //     if(i==0){
        //             continue;
        //         }
        //         else if(i % 40 == 0){
        //         a.document.write("<p style='page-break-after: always;'></p>")
        //     }
            
        // }

        // a.document.write("<table>")
        //     a.document.write("<tr style='max-width:200px; border:1px solid red;'>")
        //         a.document.write("<td style='max-width:66px'>")
        //             a.document.write("first-row-second-col");
        //         a.document.write("</td>")
        //     a.document.write("</tr>")
        // a.document.write("</table>")
        
    //    document.body.innerHTML = document.getElementById("printableDiv").innerHTML
    //    window.print()
        
        // a.document.write(`<center>${QrContents}</center>`);
        
        
        // window.print()
    //     var x = totalQuantity/numberOfItemsPerBox;
    //     int_part = Math.trunc(x); // returns 3
    //     float_part = Number((x-int_part).toFixed(2)); // return 0.2
    //     console.log(int_part)
    //     console.log(float_part)


        // let boxCounter = 0
        // for(a=0; a<(5-1); a++){
        //     boxCounter = a+1
        //     for(b=0; b<100; b++){
        //         console.log("box: "+ boxCounter + "/" + 5)
                
        //     }
        // }
        // boxCounter += 1
        // for(c=0; c<80; c++){
        //     console.log("box: "+ boxCounter + "/" + 5)
        // }
    }
</script>
<!-- QR code generator -->

<!-- qr code cdn -->
<script src="https://cdn.rawgit.com/davidshimjs/qrcodejs/gh-pages/qrcode.min.js"></script>
<!-- SWAL -->
<script src="//cdn.jsdelivr.net/npm/sweetalert2@11"></script>  
<!-- barcode cdn -->
<script src="https://cdn.jsdelivr.net/npm/jsbarcode@3.11.0/dist/JsBarcode.all.min.js"></script>
<script src="//cdn.jsdelivr.net/npm/sweetalert2@11"></script>
<!-- bootstrap -->
<!-- <script src="https://cdn.jsdelivr.net/npm/jquery@3.5.1/dist/jquery.slim.min.js" integrity="sha384-DfXdz2htPH0lsSSs5nCTpuj/zy4C+OGpamoFVy38MVBnE+IbbVYUew+OrCXaRkfj" crossorigin="anonymous"></script> -->
<script src="https://cdn.jsdelivr.net/npm/bootstrap@4.6.2/dist/js/bootstrap.bundle.min.js" integrity="sha384-Fy6S3B9q64WdZWQUiU+q4/2Lc9npb8tCaSX9FK7E8HnRr0Jz8D6OP9dO5Vg3Q9ct" crossorigin="anonymous"></script>

</body>

</html>

<?php 