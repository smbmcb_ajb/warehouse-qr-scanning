<?php 
if(!isset($_SERVER['HTTP_REFERER'])){
  // redirect them to your desired location
  header('location: ../../index.php');
  exit;
}
session_start();
if(!isset($_SESSION['EmpNum'])){
  header('location: ../../index.php');
}
header("Access-Control-Allow-Origin: {$_SERVER['HTTP_ORIGIN']}");
// echo $_SERVER['HTTP_REFERER'];
include "../connection.php";
// $input=file_get_contents("php://input");
// $decode=json_decode($input, true);

// $data = $decode['data'];
// $data = "B57U166";
// $data = $decode['data'];
// $data = "B57U166";
$resultArray = array();
$sql = "SELECT ALL
[id], 
[GOODS_CODE],
[ITEM_CODE],
[DATE_RECEIVE],
[PO],
[INVOICE],
[QTY],
[ASY_LINE],
[NUMBER_OF_BOX],
[isPrinted],
[INNER_QR_LABEL],
[BOX_QR_PRINTED_BY],
[INNER_QR_PRINTED_BY]
FROM [MA_Receiving].[dbo].[Receiving]
WHERE [DATE_RECEIVE] > '2022-11-01'";

// $sql = "WITH DEDUPE AS (
//   SELECT  id, GOODS_CODE, ITEM_CODE, DATE_RECEIVE, PO, INVOICE, QTY, ASY_LINE, NUMBER_OF_BOX, isPrinted
//         , ROW_NUMBER() OVER ( PARTITION BY PO, ITEM_CODE, GOODS_CODE, INVOICE, DATE_RECEIVE ORDER BY DATE_RECEIVE) AS OCCURENCE
//   FROM Receive$
//   )
// SELECT  * FROM DEDUPE
// WHERE
// OCCURENCE = 1";

// $sql = "SELECT * FROM Receive$";

// echo $sql;
// $data = "B57U166";
$result = sqlsrv_query($conn, $sql);
// echo $result;

if($result === false) {
  die( print_r( sqlsrv_errors(), true) );
}
    $i = 0;
    while($row = sqlsrv_fetch_array($result, SQLSRV_FETCH_ASSOC)) {
      
      // echo $row['PO'];
      // echo json_encode($resultArray, JSON_PRETTY_PRINT) . "<br>";
      $resultArray[$i]['ID'] = $row['id'];
      $resultArray[$i]['GOODS_CODE'] = $row['GOODS_CODE'];
      $resultArray[$i]['ITEM_CODE'] = $row['ITEM_CODE'];
      $resultArray[$i]['DATE_RECEIVE'] = $row['DATE_RECEIVE'];
      $resultArray[$i]['PO'] = $row['PO'];
      $resultArray[$i]['INVOICE'] = $row['INVOICE'];
      $resultArray[$i]['QTY'] = $row['QTY'];
      $resultArray[$i]['ASY_LINE'] = $row['ASY_LINE'];
      $resultArray[$i]['NUMBER_OF_BOX'] = $row['NUMBER_OF_BOX'];
      $resultArray[$i]['isPrinted'] = $row['isPrinted'];
      $resultArray[$i]['INNER_QR_LABEL'] = $row['INNER_QR_LABEL'];
      $resultArray[$i]['BOX_QR_PRINTED_BY'] = $row['BOX_QR_PRINTED_BY'];
      $resultArray[$i]['INNER_QR_PRINTED_BY'] = $row['INNER_QR_PRINTED_BY'];
      // $resultArray[$i]['MATERIAL_CODE'] = checkMaterialID($row['GOODS_CODE']);
    //   $resultArray[$i]['BOXLABEL_CHECKBOX'] = "&lt;input class='form-check-input boxLabelQR' type='checkbox' value='sample'&gt;";
    //   echo $row['PART_NAME'] . "<br>";
      $i++;
      // header('Content-type: application/json');
      
    }

    echo json_encode($resultArray, JSON_PRETTY_PRINT);
    // echo json_encode($resultArray[0], JSON_PRETTY_PRINT);

sqlsrv_close($conn);







    