<?php 
    require "1Connection.php";
    require "main_function/login.php";
?>

<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Warehouse Login</title>

    <!-- LINK PARA SA FONT AWESOME 5 FOR -->
    <!-- <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.12.0-2/css/fontawesome.min.css" />
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.12.0-2/css/all.min.css" /> -->

    <!-- EXTERNAL CSS -->
    <link rel="stylesheet" href="css/login.css" />


    <!-- FAVICON -->
    <link rel="shortcut icon" type="image/jpg" href="favicon_io/android-chrome-512x512.png"/>

</head>

<body>

    <!-- <div class="container">
    
        <div class="wrapper">
            <form method="POST" action="main_function/login.php">

                <div class="img-container">
                    <img src="images/MATech.png" alt="M.A Technology Inc.">
                </div>

                <h2>Warehouse</h2>

                <input type="text" id="email" name="ENumber" placeholder="Employee Number" required>

                <div class="pass-icon">

                    <input type="password" id="password" name="PWord" placeholder="Password" required>

                </div>
                
                <input type="submit" id="login-btn" value="Login">

                <p>Contact the CIS department for assistance if you don't have an account or forgot your password. 
                    <br> <b><i class="fa fa-phone-alt"></i> 536 </b>
                </p>
                
            </form>  

        </div>
        
        <div class="main-img"></div>

    </div> -->

        <div class="container">

            <div class="brand-logo"></div>

            <img src="/images/MATech.png" alt=""> <br>

            <span class="brand-title">Sign in to <span class="system-name">Warehouse Receiving</span></span>

            <form class="inputs" name="form" action="main_function/login.php" method="post">
                <input type="text" placeholder="Username" id="ENumber" name="ENumber" required/>
                <input type="password" placeholder="Password" id="PWord" name="PWord" required/>
                <button type="submit" name="button1">LOGIN</button>
            </form>

        </div>
    
</body>

</html>